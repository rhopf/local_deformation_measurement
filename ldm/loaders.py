from __future__ import division
from helper import *

# plotting
from matplotlib import pyplot as plt
from matplotlib import rc
fp = {'family':'serif', 'serif':['Helvetica'], 'size':'20'}
rc('font', **fp)
rc('text',  usetex=True)
rc('xtick', labelsize=20)
rc('ytick', labelsize=20)

# plot scaling
plt.rcParams['figure.figsize'] = (10.0, 7.0)
plt.rcParams['axes.grid'] = True

# modules
import sys, os, glob
import numpy as np


class Paths(object):
    '''
    Stores name and all paths for the experiment.
    '''
    def __init__(self, basepath, sensor_file_sub_path, images_folder_sub_path, ftype, geometry_file_sub_path):

        # check some formatting for basepath
        assert os.path.isabs(basepath), 'Basepath must be absolute.'
        if os.path.basename(basepath) == '':
            self.basepath = os.sep.join(basepath.split(os.sep)[:-1])
        else:
            self.basepath = basepath
        
        # check and store sensor file path and images folder
        if os.path.basename(sensor_file_sub_path) == '':
            sensor_file_sub_path = os.sep.join(sensor_file_sub_path.split(os.sep)[:-1])
            self.sensor_file     = os.path.join(self.basepath, sensor_file_sub_path)
        if os.path.isabs(sensor_file_sub_path):
            print 'Error: sensor file path must be relative to base path.'
        else:
            self.sensor_file   = os.path.join(self.basepath, sensor_file_sub_path)
        
        if os.path.basename(images_folder_sub_path) == '':
            images_folder_sub_path = os.sep.join(images_folder_sub_path.split(os.sep)[:-1])
            self.images_folder     = os.path.join(self.basepath, images_folder_sub_path)
        if os.path.isabs(images_folder_sub_path):
            print 'Error: images folder path must be relative to base path.'
        else:
            self.images_folder     = os.path.join(self.basepath, images_folder_sub_path)
            
        if os.path.basename(geometry_file_sub_path) == '':
            geometry_file_sub_path = os.sep.join(geometry_file_sub_path.split(os.sep)[:-1])
            self.geometry_file     = os.path.join(self.basepath, geometry_file_sub_path)
        if os.path.isabs(geometry_file_sub_path):
            print 'Error: geometry file path must be relative to base path.'
        else:
            self.geometry_file   = os.path.join(self.basepath, geometry_file_sub_path)
            
        # assign filetype
        self.fextension = '.' + ftype


########################### RECONFIGURE ANYTHING ###########################
def load_experiment(selection, Paths):
    '''
    Load experimental data based on the selected experiment type. This can
    be extended as much as required.
    '''

    # load mts uniaxial ("eth/imes standard")
    if selection=='mts_one_axis':
        
        exp = load_mts_one_axis(Paths)
        return exp

    # load inflation ("eth/imes standard")
    # if selection=='inflation':
    #     
    #     exp = load_inflation(Paths)
    #     return exp

        
def load_mts_one_axis(Paths):
    '''
    Load pure shear experiment for MTS machine. The "standard formatting" of all files is required.
    '''

    # set Paths and ftype
    basepath      = Paths.basepath
    images_folder = Paths.images_folder
    sensor_file   = Paths.sensor_file
    geometry_file = Paths.geometry_file

    ftype         = Paths.fextension

    # load all veddac image names
    frames = list(np.sort(glob.glob(os.path.join(images_folder, '*' + ftype))))

    # load veddac time data
    time_img = np.loadtxt(os.path.join(images_folder, 'VDCCam.log'), skiprows=3, usecols=(4,))

    # load mts sensor data
    time_mts, u1, f1, u2, f2 = parse_mts_file_with_headers(sensor_file).T

    # specimen geometry file
    width, t1, t2, t3 = np.loadtxt(geometry_file).T

    # shift and rescale time vectors
    time_mts = time_mts - time_mts[0] - 0.5 
    time_img = time_img - time_img[0]
    time_img = time_img/1000.0

    # process mts sensor data
    displ = u1 + u2
    force = (f1 + f2)/2.0
   
    # process geometry file
    width       = np.mean(width)
    thickness   = (np.mean(t1) + np.mean(t2) + np.mean(t3))/3.0
    cs_area_ref = width*thickness

    # number of data points
    n_sen = len(force)
    n_img = len(frames)

    # experiment name
    name = os.path.basename(basepath)

    # quick check
    assert len(time_img) == len(frames), 'Frames and times of frames don\'t match.'

    return {'name':name, 'frames':frames, 'time_sen':time_mts,
            'time_img':time_img,
            'displ':displ, 'force':force,
            'cs_area_ref':cs_area_ref,
            'n_sen':n_sen, 'n_img':n_img}

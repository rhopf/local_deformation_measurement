# required modules and functions
from __future__ import division
from scipy.stats import linregress
from ldm.helper import *

# plotting
from matplotlib import pyplot as plt
from matplotlib import rc
fp = {'family':'serif', 'serif':['Helvetica'], 'size':'20'}
rc('font', **fp)
rc('text',  usetex=True)
rc('xtick', labelsize=20)
rc('ytick', labelsize=20)

# plot scaling
plt.rcParams['figure.figsize'] = (10.0, 7.0)
plt.rcParams['axes.grid'] = True

# modules
import sys, os, glob
import numpy as np

# ipython widgets
from ipywidgets import widgets
from IPython.display import display

def select_reference_method():
    options = {
        'Preforce' : 0,
        'Zero slope of force': 1,
        'Maximum signal curvature: convolution': 2,
        'Set to first window value': 3,
    }

    d = widgets.Dropdown()
    d.options = options

    display(d)

    return d


def get_reference(selection, displ, force, w_start, w_end, preforce):
    '''
    Main run function. It finds the reference according to the selected method and
    returns the reference index as well as a plot for visualization.
    
    Parameters
    ----------
    selection : int
        What method is used for the reference reconstruction.
    displ : array [N_sen,]
        Vector containing measured displacements.
    w_start, w_end : array
        Data cropping: Window on which to search for the reference state. 

    Returns
    -------
    idx_sen_ref : int
        Index of the reference configuration.
    fig : mpl figure object
        Visualization
    '''
    
    if selection == 0:
        # GET REFERENCE
        idx_sen_ref, fig = get_reference_preforce(displ, force, w_start, w_end, preforce)

    if selection == 1:
        ########### PARAMETERS TO ADJUST ########### reference finder settings
        K     = 0.10                           # noise amplitude
        A     = 0.05                           # threshold multiplier
        Nmin  = int((w_end-w_start)/16 + 1)    # minimum fit array length
        Nmax  = int((w_end-w_start)/13 + 2)    # maximum fit array length
        Nstep = int((Nmax-Nmin)/5)             # step length

        # bundle params
        params_fit = (Nmin, Nmax, Nstep, w_start, w_end)
        params_ref = (A, K)
        
        # GET REFERENCE
        idx_sen_ref, slope, slope_mean, curvature, curvature_mean = get_reference_slope(displ, force, params_fit, params_ref)
        
        # plot
        fig = plot_reference_slope(displ, force, w_start, w_end, idx_sen_ref, slope, slope_mean, curvature, curvature_mean)
        
    if selection == 2:
        # GET REFERENCE
        idx_sen_ref, fig = get_reference_convolution(displ, force, w_start, w_end)
        
    if selection == 3:
        fig = plt.figure()
        idx_sen_ref = w_start
        
    # declare reference index
    print 'Reference index found at:', idx_sen_ref
    
    return idx_sen_ref, fig


def get_reference_convolution(displ, force, w_start, w_end):
    '''
    Convolution method.
    '''
    # window to use
    N  = int(w_end-w_start)
    
    # figure object and axes
    fig = plt.figure(figsize=(21, 7))
    ax  = fig.add_subplot(121)

    for i in range(2, 10, 1)[::-1]:
        # set up correct length for selected sigma
        n = int(N/float(i))

        # gaussian filter 
        xf = np.linspace(-3, 3, n)
        g  = 1/(np.sqrt(2*np.pi))*np.exp(-xf**2/(2.0))

        # normalize filter to interval
        g  = g/np.sum(g)

        # derivatives and the corresponding normalization
        dn  = 2
        _, ddg = numdiff(xf, g, dn)

        # convolve with mask
        yf  = np.convolve(force[w_start:w_end], ddg, mode='same')

        # where to chop and reference
        k   = int(N/4.0)
        ixt = np.argmax(chop(yf, k))
        
        idx_sen_ref = ixt + k + w_start
        u_ref       = displ[idx_sen_ref]

        ax.plot(chop(displ[w_start:w_end], k), chop(yf, k), 'b', lw=1.0)
    
    # format plot and add reference
    ax.set_title(r'Convolutions: Curvature')
    ax.title.set_fontsize(30)
    ax.set_xlabel(r'displacement [mm]')
    ax.set_ylabel(r'curvature [-]')
    ax.grid(color='0.25')
    y_bnds = [ax.get_ybound()[0], ax.get_ybound()[1]]
    ax.plot([u_ref, u_ref], y_bnds, '--g', label='calculated reference')
    leg = ax.legend(loc='best', prop={'size':18}, fancybox=True)
    leg.get_frame().set_alpha(0.5)
    ax.set_ylim(y_bnds)
    
    # plot data
    ax  = fig.add_subplot(122)
    ax.plot(displ[w_start:w_end], force[w_start:w_end])
    ax.set_title(r'Convolutions: Data')
    ax.title.set_fontsize(30)
    ax.set_xlabel(r'displacement [mm]')
    ax.set_ylabel(r'force [-]')
    ax.grid(color='0.25')
    y_bnds = [ax.get_ybound()[0], ax.get_ybound()[1]]
    ax.plot([u_ref, u_ref], y_bnds, '--g', label='calculated reference')
    ax.set_ylim(y_bnds)
    leg = ax.legend(loc='best', prop={'size':18}, fancybox=True)
    leg.get_frame().set_alpha(0.5)
    
    return idx_sen_ref, fig


def get_reference_preforce(displ, force, w_start, w_end, preforce):
    '''
    Preforce method. A classic...
    '''
    
    # figure object and axes
    fig = plt.figure(figsize=(10, 7))
    ax  = fig.add_subplot(111)
    
    # compute reference
    idx_sen_ref = np.argmin(np.abs(force[w_start:w_end] - preforce)) + w_start
    u_ref       = displ[idx_sen_ref]
    
    # plot data
    ax.plot(displ[w_start:w_end], force[w_start:w_end])
    ax.set_title(r'Preforce: Data')
    ax.title.set_fontsize(30)
    ax.set_xlabel(r'displacement [mm]')
    ax.set_ylabel(r'force [-]')
    ax.grid(color='0.25')
    y_bnds = [ax.get_ybound()[0], ax.get_ybound()[1]]
    ax.plot([u_ref, u_ref], y_bnds, '--g', label='calculated reference')
    ax.set_ylim(y_bnds)
    leg = ax.legend(loc='best', prop={'size':18}, fancybox=True)
    leg.get_frame().set_alpha(0.5)
    
    return idx_sen_ref, fig


def find_slope_curvature(u, f, params_fit):
    '''
    ********* FURTHER DEVELOPMENT DESIRED **********
    Auxuiliary function that calculates the slope and curvature measures,
    as defined previously by means of sweeping through a range of displacement
    increment intervals.

    Procedure
    ---------
        1) A range of deformation increments is defined. Global strain
           estimators can be used as an approximate, in order to obtain a
           corresponding index range.
        2) A global range of deformation is defined.
        3) At selected points in the global range, linear regressions are
           computed for all increment sizes. Each selected point serves as
           the starting point of the regression.
        4) Slopes and regression error (-> curvature) are returned.

    Parameters
    ----------
    u : array [N_s,]
        Vector containing measured displacements.
    f : array [N_s,]
        Vector containing measured forces.
    params : list [5,]
        Setting parameters, that define the
            Nmin:    minimum window size (displacement increment defined by index)
            Nmax:    maximum window size (displacement increment defined by index)
            Nstep:   index jump from a starting index to the next
            w_start: first starting index of the regressor
            w_end:   last starting index of the regressor

    Returns
    -------
    slope : list [n_scales, n_start_points]
        All calculated slopes for all scales of displacement increment.
    '''

    # unpack
    Nmin, Nmax, Nstep, w_start, w_end = params_fit

    # init
    slope     = list()
    curvature = list()

    # loop over all starting points
    for i in range(Nmin, Nmax, Nstep):
        mi = list()
        ri = list()

        # loop over the deformation range
        for j in range(w_start, w_end):
            mii, _, _, _, rii = linregress(u[j:j+i], f[j:j+i])
            mi.append(mii)
            ri.append(rii)

        slope.append(mi)
        curvature.append(ri)

    return slope, curvature


def get_reference_slope(displ, force, params_fit, params_ref):
    '''
    ********* FURTHER DEVELOPMENT DESIRED **********
    Main function that returns the index of the reference configuration.

    Parameters
    ----------
    displacement : array [N_s,]
        Vector containing measured displacements.
    force : array [N_s,]
        Vector containing measured forces.
    params_fit : list [5,]
        Setting parameters, that define the
            Nmin:    minimum window size (displacement increment defined by index)
            Nmax:    maximum window size (displacement increment defined by index)
            Nstep:   index jump from a starting index to the next
            w_start: first starting index of the regressor
            w_end:   last starting index of the regressor
    params_ref : list/tuple
        (A, K), with A*K setting the threshold.


    Returns
    -------
    idx_ref : float
        Index of the reference configuration.
    '''
    
    # helper function
    def find_reference_displacement_local(slope, params_ref):
        '''
        ********* FURTHER DEVELOPMENT DESIRED **********
        Auxiliary function that calculates the optimal reference index, based on
        slope data.

        Parameters
        ----------
        slope : array
            Averaged slope array.
        params: list/tuple
            (A, K), with A*K setting the threshold.
        '''
        # unpack
        A, K = params_ref

        # maximum slope
        slope_max = np.max(slope)

        # ensure the real maximum is an absolute minimum
        slope_shift = np.abs(slope - slope_max)
        
        # normalize (questionable)
        c = slope_shift/slope_shift.max()

        threshold = A*K
        for i_ref, ci in enumerate(c):
            if ci < threshold:
                break
                
        # return optimal local reference index
        return i_ref
    
    # unpack
    Nmin, Nmax, Nstep, w_start, w_end = params_fit
    
    # rename variables
    u = displ
    f = force

    # calculate slope and curvature
    slope, curvature = find_slope_curvature(u, f, params_fit)

    # compute averages
    slope_mean       = np.array(slope).mean(axis=0)
    curvature_mean   = np.array(curvature).mean(axis=0)

    # get reference index
    idx_ref = find_reference_displacement_local(slope_mean, params_ref) + w_start

    return idx_ref, slope, slope_mean, curvature, curvature_mean


def plot_reference_slope(u, force, w_start, w_end, idx_ref, slope, slope_mean, curvature, curvature_mean):
    '''
    Simple plotting function.

    Returns
    -------
    Figure object.
    '''
    # settings
    ymax_s    = 1.1*np.max((slope[0]))
    ymax_c    = 1.1*np.max((curvature[0]))
    fontsize  = 20
    labelsize = 16

    # reference
    u_ref = u[idx_ref]

    # figure and axes
    fig = plt.figure(figsize=(24, 9))
    ax  = fig.add_subplot(131)

    # plot slopes
    [ax.plot(u[w_start:(w_start+len(mi))], mi, 'b', lw=1) for mi in slope]
    ax.plot(u[w_start:(w_start + len(slope_mean))], slope_mean, 'r', lw=2, label='mean')

    # plot actual zero point
    ax.plot([u_ref, u_ref], [0, ymax_s], '--g', label='calculated reference')
    leg = ax.legend(loc=4, prop={'size':18}, fancybox=True)
    leg.get_frame().set_alpha(0.5)

    # plot settings
    ax.set_ylim([0, ymax_s])
    ax.set_title(r'Regression Sweep: Slope')
    ax.yaxis.label.set_fontsize(fontsize)
    ax.xaxis.label.set_fontsize(fontsize)
    ax.title.set_fontsize(fontsize)
    ax.title.set_y(1.01)
    ax.tick_params(axis='both', which='major', labelsize=labelsize)
    ax.grid(color='0.25')

    ax  = fig.add_subplot(132)
    [ax.plot(u[w_start:(w_start+len(mi))], mi, 'b', lw=1) for mi in curvature]
    ax.plot(u[w_start:(w_start+len(curvature_mean))], curvature_mean, 'r', lw=2, label='mean')

    ax.plot([u_ref, u_ref], [0, ymax_c], '--g', label='calculated reference')
    leg = ax.legend(loc=1, prop={'size':18}, fancybox=True)
    leg.get_frame().set_alpha(0.5)
    ax.set_ylim([0, ymax_c])
    ax.set_title(r'Regression Sweep: Curvature')
    ax.yaxis.label.set_fontsize(fontsize)
    ax.xaxis.label.set_fontsize(fontsize)
    ax.title.set_fontsize(fontsize)
    ax.title.set_y(1.01)
    ax.tick_params(axis='both', which='major', labelsize=labelsize)
    ax.grid(color='0.25')
    
    # plot data
    displ = u
    ax  = fig.add_subplot(133)
    ax.plot(displ[w_start:w_end], force[w_start:w_end])
    ax.set_title(r'Regression Sweep: Data')
    ax.yaxis.label.set_fontsize(fontsize)
    ax.xaxis.label.set_fontsize(fontsize)
    ax.title.set_fontsize(fontsize)
    ax.title.set_y(1.01)
    ax.tick_params(axis='both', which='major', labelsize=labelsize)
    ax.set_xlabel(r'displacement [mm]')
    ax.set_ylabel(r'force [-]')
    ax.grid(color='0.25')
    y_bnds = [ax.get_ybound()[0], ax.get_ybound()[1]]
    ax.plot([u_ref, u_ref], y_bnds, '--g', label='calculated reference')
    ax.set_ylim(y_bnds)
    leg = ax.legend(loc='best', prop={'size':18}, fancybox=True)
    leg.get_frame().set_alpha(0.5)

    return fig

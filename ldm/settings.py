# better division
from __future__ import division

# magics
get_ipython().magic(u'load_ext autoreload')
get_ipython().magic(u'autoreload 2')
get_ipython().magic(u'matplotlib inline')

# custom modules and settings
from IPython.display import Image
from ipywidgets import interact, interactive, RadioButtons
import ipywidgets as widgets
from IPython.display import display

# system
import os, sys, glob

# numpy
import numpy as np

# scipy
from scipy.optimize import minimize
from scipy.spatial import Delaunay

# open cv
import cv2

# custom ldm functions
sys.path.append(os.path.join(os.getcwd()))
import ldm.loaders as ldm_loaders
import ldm.tracker as ldm_trk
import ldm.kinematics as ldm_kin
import ldm.find_reference as ldm_ref
import ldm.data_plotting as ldm_dpl
import ldm.data_processing as ldm_prc
from ldm.helper import *

# matplotlib
from matplotlib import pyplot as plt
from matplotlib import animation

# settings for plotting
from matplotlib import rc
fp = {'family':'serif', 'serif':['Helvetica'], 'size':'20'}
rc('font', **fp)
rc('text',  usetex=True)
rc('xtick', labelsize=20)
rc('ytick', labelsize=20)

# plot scaling
plt.rcParams['figure.figsize'] = (10.0, 7.0)
plt.rcParams['axes.grid'] = True

# type oftions for the experiment
options_type = {
    'Monotonic Loading (also for failure)': 'loading',
    'Single Cycle'  : 'cycle',
#     'Cyclic Dynamic' : 'cyclic',
#     'Relaxation': 'relaxation',
#     'Creep' : 'creep'
}

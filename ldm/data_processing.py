# required modules and functions
from __future__ import division
from scipy.stats import linregress

# custom modules
from ldm.helper import *
import ldm.kinematics as ldm_kin
import ldm.find_reference as ldm_ref

# load binary file if availiable
# try:
#     import ldm.kinematics_bin as ldm_kin
# except ImportError:
#     print 'No binary avaliable.'

# plotting
from matplotlib import pyplot as plt
from matplotlib import rc
fp = {'family':'serif', 'serif':['Helvetica'], 'size':'20'}
rc('font', **fp)
rc('text',  usetex=True)
rc('xtick', labelsize=20)
rc('ytick', labelsize=20)

# plot scaling
plt.rcParams['figure.figsize'] = (10.0, 7.0)
plt.rcParams['axes.grid'] = True

# modules
import cv2
import sys, os, glob
import numpy as np

# ipython widgets
from ipywidgets import widgets
from IPython.display import display

def inter(action, exp):
    '''
    Function that holds all interaction methods for data trimming.
    '''

    # load all basic quantities
    cs_area_ref  = exp['cs_area_ref']

    # sensor data
    time_sen     = exp['time_sen']
    force        = exp['force']
    displ        = exp['displ']

    # image data
    frames       = exp['frames']
    time_img     = exp['time_img']

    # number of sensor samples and images
    n_sen        = exp['n_sen']
    n_img        = exp['n_img']
    
    # paths and extra data
    configpath   = exp['configpath']
    savepath     = exp['savepath'] 
    epsx_glob    = exp['epsx_glob']

    # (1) EXPERIMENT CROPPING
    if action == 'crop_exp':

        # default slider limits
        cs = 0
        ce = n_sen

        # check for existing cropping and store keyword in exp
        fname_crop = os.path.join(configpath, 'crop.dat')
        exp['fname_crop'] = fname_crop
        if os.path.exists(fname_crop):
            cs, ce = np.loadtxt(fname_crop)

        def crop_exp(idx_sen_start=cs, idx_sen_end=ce):

            # cut end
            x = time_sen[idx_sen_start:idx_sen_end]
            y = force[idx_sen_start:idx_sen_end]

            # plot it
            plt.figure(figsize=(10, 7))
            plt.plot(x, y)            
            plt.title('Set sliders to crop experiment')
            plt.xlabel('sample points')
            plt.ylabel('force [N]')
            pass
    
        w = widgets.interactive(crop_exp, idx_sen_start=(1, n_sen-1, 1), 
                                          idx_sen_end=(1, n_sen-1, 1))

    # (2) CROP REFERENCE
    if action == 'crop_ref':
        def crop_ref(idx_sen_start=0, idx_sen_end=n_sen):
            
            # cut end
            x = 100*epsx_glob[idx_sen_start:idx_sen_end]
            y = force[idx_sen_start:idx_sen_end]
            
            # plot it
            plt.figure(figsize=(10, 7))
            plt.plot(x, y)            
            plt.title('Set sliders to desired window.')
            plt.xlabel('estimated strain [\%]')
            plt.ylabel('force [N]')
            pass

        # set divider (set to 1 if issues occur)
        div = 4

        # set sliders to crop the transition area
        w = widgets.interactive(crop_ref, idx_sen_start=(1, int(n_sen/div), 1), idx_sen_end=(1, int(n_sen/div), 1))
    
    return w, exp

def cut_data(idx_sen_start, idx_sen_end, idx_img_start, idx_img_end, exp):
    '''
    Cut data in dict specific to lengths of arrays.
    '''
    n_sen = exp['n_sen']
    n_img = exp['n_img']

    for key in exp:
        if type(exp[key]) == np.ndarray:
            if len(exp[key]) == n_sen:
                if idx_sen_end == -1:
                    exp[key] = exp[key][idx_sen_start:]
                else:
                    exp[key] = exp[key][idx_sen_start:idx_sen_end]
            if len(exp[key]) == n_img:
                if idx_img_end == -1:
                    exp[key] = exp[key][idx_img_start:]
                else:
                    exp[key] = exp[key][idx_img_start:idx_img_end]

    exp['n_sen'] = len(exp['time_sen'])
    exp['n_img'] = len(exp['time_img'])

    return exp


def ip_data(strain_time, exp):
    '''
    Interpolate data in dict, all to strain time.
    '''
    stime = exp['time_sen']
    itime = exp['time_img']
    n_sen = len(stime)
    n_img = len(itime)

    for key in exp:
        if type(exp[key]) == np.ndarray:
            if len(exp[key]) == n_sen:
                exp[key] = np.interp(strain_time, stime, exp[key])
            elif len(exp[key]) == n_img:
                exp[key] = np.interp(strain_time, itime, exp[key])

    exp['n_sen'] = len(exp['time_sen'])
    exp['n_img'] = len(exp['time_img'])

    return exp


def process_coordinates(coords, main_direction, mm_per_pixel):
    '''
    Postprocessing function that reads coordinates and returns:
        1) linear strain measures
        2) principal directions (current)
        3) rigid body motion
    '''
    # reference coordinates
    coords_ref = coords[0]

    epsx      = []
    epsy      = []
    evecs     = []
    rb_angle  = []
    rb_transl = []
    residuals = []

    # loop through all tracked images and get affine map, start at 1 to skip first frame (ref)
    for coords_def in coords[1:]:
        
        # compute kinematics
        F, b, residual = ldm_kin.get_affine_transform(coords_ref, coords_def, center=True)
        R, M = ldm_kin.get_polar_decomposition(F)
        
        # compute strains and assign corect orientations with respect to image axes
        if main_direction == 'horizontal':
            ex, ey, evc = ldm_kin.get_principal_strains(F)
            evec        = evc
        else:
            ey, ex, evc = ldm_kin.get_principal_strains(F)
            evec        = np.array([evc[1], evc[0]])
        
        # append data
        rb_angle.append(180.0*np.arcsin(R[1][0])/np.pi)
        rb_transl.append(b)
        epsx.append(ex)
        epsy.append(ey)
        evecs.append(evc)
        residuals.append(residual)

    # to numpy array
    epsx      = np.array(epsx)
    epsy      = np.array(epsy)
    evecs     = np.array(evecs)
    rb_angle  = np.array(rb_angle)
    rb_transl = mm_per_pixel*np.array(rb_transl)
    residuals = np.array(residuals)

    # add zero strain at start
    epsx      = np.insert(epsx, 0, 0)
    epsy      = np.insert(epsy, 0, 0)
    rb_angle  = np.insert(rb_angle, 0, 0)
    rb_transl = np.vstack(([0, 0], rb_transl))

    return epsx, epsy, evecs, rb_angle, rb_transl, residuals


def percentile_filter_coords(exp, level=2.0, pct=95):
    '''
    Apply iterative, percentile filterings (f.e. very efficient at removing specular light issues).
    '''
    # unpack
    main_direction = exp['main_direction']
    mm_per_pixel   = exp['mm_per_pixel']
    residuals = exp['residuals']
    coords    = exp['coords']
    
    # init
    res_motion_ave = np.abs(residuals.mean(axis=0))

    # init filter
    res_motion_ave_med = np.percentile(res_motion_ave, pct)
    idx_bad = np.where([i for i in res_motion_ave > res_motion_ave_med])

    i_iter = 0

    # iterative application of a percentile filter
    while(np.max(res_motion_ave)>level):
       
        # calculate center of gravity (CG) and residual weighted CG
        cg          = coords[0].mean(axis=0)
        cg_weighted = np.sum(coords[0]*np.c_[res_motion_ave, res_motion_ave], axis=0)/np.sum(res_motion_ave)
        cg_dist     = np.linalg.norm(cg - cg_weighted)
       
       # print some stats
        i_iter += 1
        print 'Iter nr. {a} \tPoints deleted: {b} \tPoints remaining: {c} \tMax. res.: {d} \t Global affinity: {e}'.format(a=i_iter, b=len(idx_bad[0]), c=len(coords[0]), d=np.max(res_motion_ave).round(2), e=cg_dist.round(3))

        # check for at least 20 remaining points
        if len(coords[0]) <= 20:
            print 'Truncated at', i, 'th iteration. Remaining points:', len(coords[0])
            break
            
        if len(idx_bad[0]) == 0:
            print 'Filter with current percentile doesn\'t resolve vector any further.'
            print 'Truncated at', i, 'th iteration. Remaining points:', len(coords[0])
            break
        
        # delete bad coordinates
        coords  = np.delete(coords, idx_bad, axis=1)

        # process coordinates
        epsx, epsy, evecs, rb_angle, rb_transl, residuals = process_coordinates(coords, main_direction, mm_per_pixel)

        # averages and median
        res_motion_ave     = np.abs(residuals.mean(axis=0))
        res_motion_ave_med = np.percentile(res_motion_ave, pct)
        
        # read out indices to discard
        idx_bad = np.where([i for i in res_motion_ave > res_motion_ave_med])

    # print last stats
    i_iter += 1

    # calculate center of gravity (CG) and residual weighted CG
    cg          = coords[0].mean(axis=0)
    cg_weighted = np.sum(coords[0]*np.c_[res_motion_ave, res_motion_ave], axis=0)/np.sum(res_motion_ave)
    cg_dist     = np.linalg.norm(cg - cg_weighted)

    print 'Iter nr. {a} \tPoints deleted: {b} \tPoints remaining: {c} \tMax. res.: {d} \t Global affinity: {e}'.format(a=i_iter, b=len(idx_bad[0]), c=len(coords[0]), d=np.max(res_motion_ave).round(2), e=cg_dist.round(3))

    # add to dict
    if i_iter > 0:
        exp['epsx_loc']     = epsx
        exp['epsy_loc']     = epsy

        # evecs, rbm, coords and residuals
        exp['coords']    = coords
        exp['residuals'] = residuals
        exp['rbm']       = dict(rot=rb_angle, transl=rb_transl)
        exp['evecs']     = evecs

    # add center
    exp['center']    = coords.mean(axis=1)

    return exp

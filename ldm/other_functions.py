# required modules and functions
import matplotlib.pyplot                 as plt
import numpy                             as np
import numpy.ma                          as ma
import mpl_toolkits.axes_grid1.axes_size as Size

from matplotlib                           import patches
from matplotlib                           import rc
from scipy.special                        import xlogy
from scipy.spatial                        import ConvexHull
from scipy.spatial                        import Delaunay
from scipy.optimize                       import leastsq
from scipy.optimize                       import minimize
from scipy.signal                         import fftconvolve
from scipy.ndimage.filters                import gaussian_filter
from astropy.convolution                  import convolve
from astropy.convolution                  import Gaussian2DKernel
from mpl_toolkits.axes_grid1.axes_divider import HBoxDivider

from ldm.settings import*
#######################################################################################################

'''
A list of the typical radial basis functions and their derivatives. They are univariate functions and depend only on the distance from a center (variable r) and from a shape parameter (variable epsilon) which should be tuned in each application in order to achieve better performances.

Multiquadrics with very small values of epsilon are typically the best suited for many applications when exact interpolation is seeked. Flatness (epsilon dependent) of RBF leads to higher condition numbers for the interpolation matrix and therefore to instability. When a smoother solution is instead sought after, it is better to use larger values of epsilon.

Functions are vectorized, therefore they can be applied on nd-arrays.
'''

def multiquadric(r, epsilon):
    return -np.sqrt(epsilon**2 + r**2)
def multiquadricd(r, epsilon):
    return -r*(1.0/np.sqrt(epsilon**2 + r**2))

def inverse_multiquadric(r, epsilon):
    return 1.0/np.sqrt((1.0/epsilon*r)**2 + 1)
def inverse_multiquadricd(r, epsilon):
    return -(r/(epsilon)**2)*(1/(1 + (r/epsilon)**2)**(1.5))

def gaussian(r, epsilon):
    return np.exp(-(1.0/epsilon*r)**2)
def gaussiand(r, epsilon):
    return -2*np.exp(-(1.0/epsilon*r)**2)*(r/(epsilon)**2)

def linear(r, epsilon):
    return -r
def lineard(r, epsilon):
    return -r/r

def cubic(r, epsilon):
    return r**3
def cubicd(r, epsilon):
    return 3*r**2

def quintic(r, epsilon):
    return r**5
def quinticd(r, epsilon):
    return 5*r**4

def thin_plate(r, epsilon):
    return xlogy(r**2, r)
def thin_plated(r, epsilon):
    return 2*r*np.log(r) + r

'''
Wendland's function (and its derivative) is a compactly supported radial basis function: that is, outside the circle defined by r/epsilon is 0. For good interpolation accuracy at least 50 points should be within the support.

This kind of function can be a very good choice when dealing with very large problems since it leads to positive definite banded interpolation matrices. Such properties would allow factorization of the linear system and shorter computation times, or the use of iterative methods which are based on PD matrices.

There are of course other available choices for compactly supported functions. Wendland of order 4 (the one here implemented) is only one of the alternatives.

The function is vectorized, therefore it can be applied on nd-arrays.
'''

def wendland(r, epsilon):
    r  = np.asarray(r)
    y  = np.zeros(r.shape)
    y += ((r >= 0) & (r/epsilon < 1)) * ((1-r/epsilon)**4* (1+4*r/epsilon))
    y += ((r*epsilon >= 1))           * (0*r)
    return y
def wendlandd(r, epsilon):
    r = np.asarray(r)
    y = np.zeros(r.shape)
    y += ((r >= 0) & (r*epsilon < 1)) * (-20*r/(epsilon^5)*(epsilon-r)**3)
    y += ((r*epsilon >= 1))           * (0*r)
    return y

'''
A bunch of useful functions for computing euclidean norms and distances.
'''

def euclidean_norm(x1, x2):
    '''
    Input:  x1 and x2 are (N_dim x N_points) arrays.
    Output: (1 x N_points) array with point-wise euclidean norm between x1 and x2.
    '''
    return np.sqrt( ((x1 - x2)**2).sum(axis=0) )

def call_norm(x1, x2, norm):
    '''
    Input:  x1 and x2 are (N_dim x N_points) arrays.
    Output: (N_points x N_points) symmetric matrix with element (i,j) as norm between point i
    in x1 and point j in x2. Typically euclidean norm is used.
    '''
    if len(x1.shape) == 1:
            x1 = x1[np.newaxis, :]
    if len(x2.shape) == 1:
            x2 = x2[np.newaxis, :]
    x1 = x1[..., :, np.newaxis]
    x2 = x2[..., np.newaxis, :]
    return norm(x1, x2)

def distance(x1,x2):
    '''
    Input:  x1 and x2 are (N_dim x N_points) arrays.
    Output: (N_points x N_points) symmetric matrix with element (i,j) as algebraic distance between
    point i in x1 and point j in x2.
    '''
    if len(x1.shape) == 1:
            x1 = x1[np.newaxis, :]
    if len(x2.shape) == 1:
            x2 = x2[np.newaxis, :]
    x1 = x1[..., :, np.newaxis]
    x2 = x2[..., np.newaxis, :]
    return (x1 - x2).sum(axis=0) 

'''
A list of objective functions for the solution methods of RBFs based on an optimization process. The optimization is formulated as minimization of the sum of squared residuals.
'''
fun = lambda x,A: np.dot(A,x)

def tikhonov(x,A,f,l):
    '''
    Objective function with Tikhonov regularization. Formulation as squared sum of residuals.
    
    f(x) = ||Ax-f||^2 + ||l*x||^2
    '''
    first_term  = (np.linalg.norm(fun(x,A)-f))**2
    second_term = (np.linalg.norm(l*x))**2
    return first_term + second_term

def tikhonov_and_penalizer(x,A,f,l,s):
    '''
    Objective function with Tikhonov regularization. Formulation as squared sum of residuals.
    The unknow variable x is now built as follows:
    
    x = (w d)^T
    
    where w are the weights and d the function values without noise.
    The formulation is penalized and it can be and alternative to terminating earlier the
    minimization of tikhonov. Objective function is:
    
    f(w,d) = ||Ax-d||^2 + ||l*x||^2 + ||s*(f-d)||^2
    '''
    dim         = len(f)
    first_term  = (np.linalg.norm(fun(x[:dim],A)-x[dim:]))**2
    second_term = (np.linalg.norm(l*x[:dim]))**2
    third_term  = (np.linalg.norm(s*(f-x[dim:])**1))**2
    return first_term + second_term + third_term
    
def LSM(x,A,f,l):
    '''
    Formulation of a Least Squared Method problem as input for the python LSM optimization function
    '''
    return fun(x,A)-f
    
'''
A couple of dictionaries for simplifying the code
'''
functions   = {'multiquadric': multiquadric, 'inverse_multiquadric': inverse_multiquadric,
              'gaussian': gaussian, 'linear': linear, 'cubic': cubic, 'quintic': quintic,
              'thin_plate': thin_plate, 'wendland': wendland}
derivatives = {'multiquadricd': multiquadricd, 'inverse_multiquadricd': inverse_multiquadricd,
              'gaussiand': gaussiand, 'lineard': lineard, 'cubicd': cubicd, 'quinticd': quinticd,
              'thin_plated': thin_plated, 'wendlandd': wendlandd}


#######################################################################################################


class RBF(object):
    '''
    Radial Basis Function class
    
    Input parameters:
    1) coordinates of scatter data as 1-D arrays (i.e. x,y,z...)
    2) function values on scatter data as 1-D array (i.e. d)
    3) different options:
        - function: type of radial basis function to be used (default is 'multiquadric')
        - norm: type of norm to be used (default is euclidean norm)
        - epsilon: shape parameter value (defualt is average distance between scatter data points)
        - regularizer: regularization parameter (defualt is 0.0)
        - augmentation (default is 'none'): polynomial term added to linear combination of RBFs.
          Possible orders are 'none', 'constant', 'linear', 'quadratic' and 'cubic'
        - condition_number: condition number limit (default is 10^18)
        - penalizer (default is None): penalizer in the sum of squared residual objective function
        - terminate (defualt is None): prescribed error on sum of squared residuals
        - localization = dictionary for changing locally properties of interpolation. Items are 
          'patch', 'RBF' and 'epsilon_loc'. It is advised not to use this feature.
        
    Output arguments of the class are:
    xi              = coordinates of points
    di              = values of function at xi
    N               = number of points of scatter
    norm            = type of norm used
    r               = symmetric matrix where r_i,j is the euclidean norm between points i and j
    regularizer     = value of regularization parameter
    epsilon         = value of shape parameter
    terminate       = value of tolerance for the norm of the gradient of the optimization problem
    penalizer       = penalizer in the formulation of the penalized least of squared method
    phi             = type of radial base function used
    refinement      = dictionary for changing local interpolation properties. Items are 'index' of
                      points, 'RBF' and 'epsilon_loc' value 
    A               = complete interpolation matrix
    A0              = interpolation matrix from RBF (Aij = phi(xi-xj)). It is exactly A when no
                      polynomial is augumented
    cond            = condition number of interpolation matrix
    porder          = order of polynomial augumented
    weights         = vector of weights of linear combination
    lambdas         = weights of RBF
    mus             = weights of polynomial term
    
    
    For a sample application example read the docstring of rbfinterp.
    '''
    def __init__(self, *args, **kwargs):
        
        # Initialization of scatter data quantities
        self.xi = np.asarray([np.asarray(a, dtype=np.float_).flatten() for a in args[:-1]])
        self.N  = self.xi.shape[-1]
        self.di = np.asarray(args[-1]).flatten()
        
        # Check if size of arrays is consistent
        if not all([x.size == self.di.size for x in self.xi]):
            raise ValueError("All arrays must be of equal length.")
        
        # Computation of distance matrix
        self.norm = kwargs.pop('norm', self._euclidean_norm)
        self.r    = self._call_norm(self.xi, self.xi)
        
        # Initialization of shape parameter
        self.epsilon = kwargs.pop('epsilon', None)
        if self.epsilon is None:
                self.dim     = self.xi.shape[0]
                ximax        = np.amax(self.xi, axis=1)
                ximin        = np.amin(self.xi, axis=1)
                edges        = ximax-ximin
                edges        = edges[np.nonzero(edges)]
                self.epsilon = np.power(np.prod(edges)/self.N, 1.0/edges.size)
        
        # Initialization of regularization parameter and RBF type
        self.regularizer     = kwargs.pop('regularizer', 0.0)
        self.phi             = kwargs.pop('function', 'multiquadric')
        self.phi             = functions[self.phi]
        
        # Computation of interpolation matrix
        self.A   = self.phi(self.r, self.epsilon)
        
        # Modification of shape parameter and RBF type on a local region of the scatter
        self.localization = kwargs.pop('localization', None)
        if self.localization is None:
            self.epsilon = self.epsilon
        else:
            local_index           = select_region(self.xi, self.localization['patch'])
            local_phi             = self.localization['RBF'] 
            local_epsilon         = self.localization['epsilon_loc']
            self.refinement       = {'index':local_index, 'RBF':local_phi,
                                     'epsilon_loc': local_epsilon}
            A_local               = local_phi(self.r, local_epsilon)
            self.A[:,local_index] = A_local[:,local_index]
            self.A[local_index,:] = A_local[local_index,:]
        
        self.A0  = self.A
        
        # Computation of polynomial term    
        self.augmentation = kwargs.pop('augmentation', 'none')
        if self.augmentation == 'none':
            self.A      = self.A
            self.di     = self.di
            self.porder = None
        else:
            if self.augmentation   == 'constant':
                self.porder = 0
            elif self.augmentation == 'linear':
                self.porder = 1
            elif self.augmentation == 'quadratic':
                self.porder = 2
            elif self.augmentation == 'cubic':
                self.porder = 3
            else:
                raise ValueError("Invalid polynomial degree.")
            
            self.P = polynomial(self.xi, self.porder)
            
            # Polynomial augmentation to linear system
            # Addition of conditions for problem well-posedness
            self.A  = expand_linear_system(self.A, self.P)
            self.di = expand_vector(self.di, self.P)
            
        # Computation of condition number
        self.cond    = np.linalg.cond(self.A)
        self.condmax = kwargs.pop('condition_number', 10.0**18)
        test_cond    = 0
        if self.cond > 10.0**12:
            print 'WARNING: Condition number larger than 10^12. Results are affected by numerical errors!'
            test_cond = 1

        # Solution of linear system
        self.terminate = kwargs.pop('terminate', None)
        self.penalizer = kwargs.pop('penalizer', None)
        if self.terminate is None or self.terminate == 0:
            if self.regularizer is None or self.regularizer == 0:
                # Direct solver
                self.weights = np.linalg.solve(self.A, self.di)
            else: 
                if self.penalizer is None or self.penalizer == 0:
                    # Explicit solution after  Tikhonov regularization        
                    regul_A      = (np.dot(self.A.T,self.A) + self.regularizer**2*np.eye(len(self.di)))
                    regul_d      = np.dot(self.A.T, self.di)
                    self.weights = np.linalg.solve(regul_A, regul_d)
                    self.A       = regul_A
                    self.cond    = np.linalg.cond(self.A)
                    if test_cond == 1:
                        if self.cond < self.condmax:
                            print 'System corrected and condition number again smaller than the maximum recommended.'
                        else:
                            print 'Condition number still larger than the maximum recommended.'
                            if self.cond > self.condmax:
                                raise ValueError("Condition number larger than the maximum allowed.")
                    
                else:
                    # Find exact minimum of penalized squared sum of residuals objective
                    x0           = np.ones(len(self.di)*2)
                    res          = minimize(tikhonov_and_penalizer,x0, method='BFGS',
                                            args=(self.A, self.di,self.regularizer, self.penalizer),
                                            options={'disp':True})
                    sol          = res.x
                    self.weights = sol[:len(self.di)]
                    self.distar  = sol[len(self.di):]
                    
        else:
            # Find approximated minimum of squared sum of residuals objective
            # Algorithm stops when norm of gradient equals prescribed tolerance (self.terminate)
            x0           = np.zeros(len(self.di)).T
            self.res       = leastsq(LSM, x0, args=(self.A,self.di,self.regularizer),
                                   ftol = self.terminate)
            self.weights  = self.res[0]    

        # Partition of weights vector
        self.lambdas = self.weights[:self.N]
        self.mus     = self.weights[self.N:]
    
    # Following functions are same as before, but for internal class use only
    def _call_norm(self, x1, x2):
        if len(x1.shape) == 1:
            x1 = x1[np.newaxis, :]
        if len(x2.shape) == 1:
            x2 = x2[np.newaxis, :]
        x1 = x1[..., :, np.newaxis]
        x2 = x2[..., np.newaxis, :]
        return self.norm(x1, x2)

    def _euclidean_norm(self, x1, x2):
        return np.sqrt( ((x1 - x2)**2).sum(axis=0) )
    
    
#######################################################################################################


def rbfinterp(*args, **kwargs):
    '''
    Create the interpolated/approximated function using weights computed in the RBF class
    
    Input parameters:
    1) grid coordinates where to assign function values (e.g. X, Y,...)
    2) coordinates of original scatter data as tuple (e.g. (x, y,...) )
    3) weights of the RBF
    4) epsilon value (shape parameter)
    5) function type (default is multiquadric). Must be consistent with RBF.phi
    6) norm type (default is euclidean norm)
    7) polynomial augmentation (default is 'none'). Maximum degree is third and maximum dimensionality
       is 3-D
    
    Output:
    Function values on the grid
    
    
    
    EXAMPLES:
    
    In 1-D:
    
    x       = np.linspace(-1,1,100)
    y       = [np.sin(xx) for xx in x]
    s       = np.random.uniform(-1.,1., 10)
    ys      = [np.sin(xx) for xx in s]
    rbf     = RBF(s, ys, regularizer=0.0, function='multiquadric', augmentation='none')
    weights = rbf.weights
    epsilon = rbf.epsilon
    interp  = rbfinterp(x, (s), weights, epsilon, function='multiquadric', augmentation='none')
    
    In 2-D:
    
    x       = np.random.rand(20)*4.0-2.0
    y       = np.random.rand(20)*4.0-2.0
    z       = x*np.exp(-x**2-y**2)
    ti      = np.linspace(-2.0, 2.0, 100)
    XI, YI  = np.meshgrid(ti, ti)
    F       = XI*np.exp(-XI**2-YI**2)
    rbf     = RBF(x, y, z)
    weights = rbf.weights
    epsilon = rbf.epsilon
    ZI      = rbfinterp(XI, YI, (x,y), weights, epsilon)
    
    N.B. When function has to be evaluated only in one point, it as well has to be expressed as grid
    variable of 1x1 dimension:
    t0         = -0.225
    t0         = -0.5
    X0, Y0     = np.meshgrid(t0,t0)
    singl_eval = rbfinterp(X0, Y0, (x,y), weights, epsilon)
    Evaluating function on a NxN grid is slower that doing it with a for loop on N^2 1x1 grid points.
    However, if we think of 2-dimensional functions, computing values on grids has however the
    advantage of being easily handled for contouring reasons and of allowing the use of local filtering
    techiques as it is possible on images.
    '''
    # Initialization of quantities
    scatter = np.asarray(args[-3])
    weights = np.asarray(args[-2]).flatten()
    epsilon = np.asarray(args[-1]).flatten()
    grid    = np.asarray([np.asarray(a, dtype=np.float_).flatten() for a in args[:-3]])
    phi     = kwargs.pop('function', 'multiquadric')
    phi     = functions[phi]
    norm    = kwargs.pop('norm', euclidean_norm)
    
    # Check if size of arrays is consistent
    if not all([x.shape == y.shape for x in grid for y in grid]):
        raise ValueError("All arrays must be of equal length.")
    
    shp           = args[0].shape
    xa            = np.asarray([a.flatten() for a in grid], dtype=np.float_)
    r             = call_norm(xa, scatter, norm)
    interp_matrix = phi(r, epsilon)
    
    refinement    = kwargs.pop('refinement', None)
    if refinement is not None:
        local_index                  = refinement['index']
        local_phi                    = refinement['RBF'] 
        local_epsilon                = refinement['epsilon_loc']
        A_local                      = local_phi(r, local_epsilon)
        interp_matrix[:,local_index] = A_local[:,local_index]
        interp_matrix[local_index,:] = A_local[local_index,:]
               
    
    # Polynomial term augmentation 
    augmentation = kwargs.pop('augmentation', 'none')
    if augmentation == 'none':
        interp_matrix = interp_matrix
    else:
        if augmentation   == 'constant':
            porder = 0
        elif augmentation == 'linear':
            porder = 1
        elif augmentation == 'quadratic':
            porder = 2
        elif augmentation == 'cubic':
            porder = 3
        else:
            raise ValueError("Invalid polynomial degree.")

        P             = polynomial(grid, porder)
        interp_matrix = np.hstack([interp_matrix, P])
    
    # Computation of function values on the grid
    new_values = np.dot(interp_matrix, weights).reshape(shp)
    
    return new_values


#######################################################################################################


def rbfinterpd(*args, **kwargs):
    '''
    Function similar to rbfinterp but for approximation of one of the first derivatives of an 
    n-dimensional function. It can also be used for 1-dimensional functions and in that case it gives
    the first derivative.
    
    N.B. So far, the function has been implemented to compute first derivatives of maximum 3-D 
    functions.
    
    EXAMPLES:
    
    In 1-D:
    
    x       = np.linspace(-1,1,100)
    y       = [np.sin(xx) for xx in x]
    s       = np.random.uniform(-1.,1., 10)
    ys      = [np.sin(xx) for xx in s]
    rbf     = RBF(s, ys, regularizer=0.0, function='multiquadric', augmentation='none')
    weights = rbf.weights
    epsilon = rbf.epsilon
    dydx    = rbfinterpd(x, (s), weights, epsilon, function='multiquadricd', augmentation='none')
    
    In 2-D:
    
    x       = np.random.rand(20)*4.0-2.0
    y       = np.random.rand(20)*4.0-2.0
    z       = x*np.exp(-x**2-y**2)
    ti      = np.linspace(-2.0, 2.0, 100)
    XI, YI  = np.meshgrid(ti, ti)
    rbf     = RBF(x, y, z)
    weights = rbf.weights
    epsilon = rbf.epsilon
    dZIdy   = rbfinterpd(XI, YI, (x,y), weights, epsilon, derivative='dy')
    '''
    
    # Initialization of quantities
    scatter = np.asarray(args[-3])
    weights = np.asarray(args[-2]).flatten()
    epsilon = np.asarray(args[-1]).flatten()
    grid    = np.asarray([np.asarray(a, dtype=np.float_).flatten() for a in args[:-3]])
    phi     = kwargs.pop('function', 'multiquadric')
    phi     = phi+'d'
    phi     = derivatives[phi]
    norm    = kwargs.pop('norm', euclidean_norm)
    deriv   = kwargs.pop('derivative', 'dx')
    
    # Check is size of arrays is consistent
    if not all([x.shape == y.shape for x in grid for y in grid]):
        raise ValueError("All arrays must be of equal length.")
    
    # Computation of dimensionality of the problem
    shp  = args[0].shape
    ndim = grid.shape[0]
    
    # Check of maximum dimensionality allowed
    if ndim > 3:
        raise ValueError("Current implementation can be used only for functions up to 3 dimensions.")
    
    # Computation of grid arrays and distance matrix
    xa   = np.asarray([a.flatten() for a in grid], dtype=np.float_)
    r    = call_norm(xa, scatter, norm)
    
    # Computation of interpolation matrix with chain rule for the derivative
    if ndim == 1:
        drd      = np.divide(distance(xa, scatter),r)
        NAN      = np.isnan(drd)
        drd[NAN] = 0
    else:
        if deriv == 'dx':
            n = 0
        elif deriv == 'dy':
            n = 1
        elif deriv == 'dz':
            n = 2
            if ndim == 2:
                raise ValueError("Inconsistency between number of dimensions and derivatives.")
        else:
            raise ValueError("Derivative not understood.")
        drd       = np.divide(distance(xa[n], scatter[n]),r)
        NAN       = np.isnan(drd)
        drd[NAN]  = 0
    interp_matrix = np.multiply(phi(r, epsilon),drd)
    
    refinement    = kwargs.pop('refinement', None)
    if refinement is not None:
        local_index                  = refinement['index']
        local_phi                    = multiquadricd #refinement['RBF'] 
        local_epsilon                = refinement['epsilon_loc']
        A_local                      = np.multiply(local_phi(r, local_epsilon),drd)
        interp_matrix[:,local_index] = A_local[:,local_index]
        interp_matrix[local_index,:] = A_local[local_index,:]
            
    # Polynomial augmentation
    augmentation = kwargs.pop('augmentation', 'none')
    if augmentation == 'none':
        interp_matrix = interp_matrix
    else:
        if augmentation   == 'constant':
            porder = 0
        elif augmentation == 'linear':
            porder = 1
        elif augmentation == 'quadratic':
            porder = 2
        elif augmentation == 'cubic':
            porder = 3
        else:
            raise ValueError("Invalid polynomial degree.")

        P             = dpolynomial(grid, porder, deriv)        
        interp_matrix = np.hstack([interp_matrix, P])

    # Computation of function values on the grid   
    new_values = np.dot(interp_matrix, weights).reshape(shp)
    
    return new_values


#######################################################################################################


class NLCM(object):
    '''
    A class for computing all the useful quantities from the theory of Non Linear Continuum Mechanics.
    
    Arguments of the class are:
    - F        Deformation tensor 
    - C        Right Cauchy-Green deformation tensor 
    - B        Left Cauchy-Green deformation tensor 
    - I1       First invariant of C
    - I2       Second invariant of C
    - I3       Third invariant of C
    - E        Green-Lagrange strain tensor
    - R        Rotational tensor
    - M        Right stretch tensor
    - N        Left stretch tensor
    - NE       Nominal strain consistent with Abaqus notation
    - LE       Logarithmic strain consistent with Abaqus notation
    - lambda1  Stretch along first principal axis
    - lambda2  Stretch along second principal axis
    - eigv1    First eigenvector of C
    - eigv2    Second eigenvector of C
    '''
    def __init__(self, F):
        
        # Cauchy-Green deformation tensors and invariants
        self.F                 = F
        self.C                 = np.dot(self.F.T, self.F)
        self.B                 = np.dot(self.F, self.F.T)
        self.I1                = np.trace(self.C)
        self.I2                = 0.5*( (np.trace(self.C))**2 - np.trace(np.dot(self.C,self.C)) )
        self.I3                = np.linalg.det(self.C)

        # Green-Lagrange strain tensor
        self.E                 = 0.5*(self.C-np.eye(2,2))
        
        # Computation of eigenvalues and eigenvectors of C
        ev, evec               = np.linalg.eigh(self.C)
        l1                     = np.sqrt(ev[0])
        l2                     = np.sqrt(ev[1])
        e1, e2                 = evec.T
        self.lambda1           = l1
        self.lambda2           = l2
        self.eigv1, self.eigv2 = evec.T

        # Symmetric right stretch tensor
        self.M   = np.array([[l1*e1[0]*e1[0] + l2*e2[0]*e2[0],
                              l1*e1[1]*e1[0] + l2*e2[1]*e2[0]], 
                            [l1*e1[0]*e1[1]  + l2*e2[0]*e2[1],
                             l1*e1[1]*e1[1]  + l2*e2[1]*e2[1]]])
        
        # Orthogonal rotational tensor
        self.R   = np.dot(self.F, np.linalg.inv(self.M))
        
        # Symmetric left stretch tensor
        ev, evec               = np.linalg.eigh(self.B)
        l1                     = np.sqrt(ev[0])
        l2                     = np.sqrt(ev[1])
        e1, e2                 = evec.T        
        self.N   = np.array([[l1*e1[0]*e1[0] + l2*e2[0]*e2[0],
                              l1*e1[1]*e1[0] + l2*e2[1]*e2[0]], 
                            [l1*e1[0]*e1[1]  + l2*e2[0]*e2[1],
                             l1*e1[1]*e1[1]  + l2*e2[1]*e2[1]]])
        
        # Nominal strain consistent with Abaqus notation
        self.NE  = np.array([[(l1-1)*e1[0]*e1[0] + (l2-1)*e2[0]*e2[0],
                              (l1-1)*e1[1]*e1[0] + (l2-1)*e2[1]*e2[0]], 
                            [(l1-1)*e1[0]*e1[1]  + (l2-1)*e2[0]*e2[1],
                             (l1-1)*e1[1]*e1[1]  + (l2-1)*e2[1]*e2[1]]])
        
        # Logarithmic strain
        self.LE  = np.array([[np.log(l1)*e1[0]*e1[0] + np.log(l2)*e2[0]*e2[0],
                              np.log(l1)*e1[1]*e1[0] + np.log(l2)*e2[1]*e2[0]], 
                            [np.log(l1)*e1[0]*e1[1]  + np.log(l2)*e2[0]*e2[1],
                             np.log(l1)*e1[1]*e1[1]  + np.log(l2)*e2[1]*e2[1]]])

        
#######################################################################################################


def in_hull(points, scatter):
    '''
    Check if (N_points x N_dim array) points is contained in the hull created with scatter,
    (N_points x N_dim array).
    '''
    if not isinstance(scatter, Delaunay):
        hull = Delaunay(scatter)

    return hull.find_simplex(points)>=0

########################################################################################################

def adapt_grid(X, Y, scatter, direction):
    '''
    Assign Nan value to elements in the grid matrices X and Y if they lie outside or inside (according
    to "direction") the hull created with scatter.
    
    EXAMPLE:
    Reshape grids X and Y so to have Nan within a circle of radius r
    
    r      = 0.075
    t      = 2*np.pi*np.linspace(0,1,100)
    x      = r*np.cos(t)
    y      = r*np.sin(t)
    circle = np.vstack([x,y]).T
    Xn,Yn  = adapt_grid(X,Y,circle,direction='out')
    '''
    if not X.shape == Y.shape:
        raise ValueError("Grid coordinates must have same dimensions")
    r,c = X.shape
    for i in np.arange(r):
        for j in np.arange(c):
            pnt = np.array([X[i,j],Y[i,j]])
            if direction == 'in':
                if in_hull(pnt, scatter) == False:
                    X[i,j] = np.nan
                    Y[i,j] = np.nan
            if direction == 'out':
                if in_hull(pnt, scatter) == True:
                    X[i,j] = np.nan
                    Y[i,j] = np.nan
    return X,Y


#######################################################################################################


def min_dist_nodes(nodes, dist, n, nD):
    '''
    Commodity function for sub-sampling an original set of nodes (N_points x N-dim+1 array) using a
    minimum distance (variable "dist") parameter. Termination criterion is length n of output array.
    '''
    coords     = nodes[:,-nD:]
    new_coords = np.array([])
    new_coords = np.hstack([new_coords, coords[0,:]])
    new_nodes  = np.array([0])
    
    for i in range(len(coords)-1):
        temp  = np.ones([1,2])
        temp  = temp*coords[i+1]
        distM = euclidean_norm(new_coords.T, temp.T)
        if (distM > dist).all() == True:
            new_coords = np.vstack([new_coords, temp])
            new_nodes  = np.vstack([new_nodes, i+1])
        if len(new_nodes) == n:
            break;
    
    print 'Last node added: ', i+2
    print 'Number of sampled nodes: ', len(new_nodes)
    
    return new_coords, new_nodes
        

#######################################################################################################


def polynomial(points, degree):
    '''
    Evaluation on the given points of a complete polynomial of prescribed degree.
    Implemented for degrees up to the third and for at most 3-D problems.
    
    Input:
    - points: ndim x npoints array
    - degree: scalar
    
    Output:
    - polynomial: npoints x nmonomials
    '''
    dim      = points.shape[0]
    npoints  = points.shape[-1]   
    constant = np.ones([npoints, 1])
         
    if dim > 1:
        x         = np.ones([npoints,1])
        y         = np.ones([npoints,1])
        x[:,0]    = points[0]
        y[:,0]    = points[1]
        xy        = np.multiply(x,y)
        x2y       = np.multiply(x**2,y)
        xy2       = np.multiply(x,y**2)
    if dim == 3:
        z         = np.ones([npoints,1])
        z[:,0]    = points[2]
        yz        = np.multiply(y,z)
        xz        = np.multiply(x,z)
        x2z       = np.multiply(x**2,z)
        y2z       = np.multiply(y**2,z)
        xz2       = np.multiply(x,z**2)
        yz2       = np.multiply(y,z**2)
    
    if degree == 0:
        polynomial = np.hstack([constant])
    elif degree == 1:
        polynomial = np.hstack([constant, points.T])
    elif degree == 2:
        if dim == 1:
            polynomial = np.hstack([constant, points.T, np.power(points,2).T])   
        elif dim == 2:
            polynomial = np.hstack([constant, points.T, xy, (points**2).T])         
        elif dim == 3:
            polynomial = np.hstack([constant, points.T, xy, yz, xz, (points**2).T])           
        else:
            raise ValueError("Dimension order not supported.")
    elif degree == 3:
        if dim == 1:
            polynomial = np.hstack([constant, points.T, (points**2).T, (points**3).T])
        elif dim == 2:
            polynomial = np.hstack([constant, points.T, xy, (points**2).T, x2y, xy2, (points**3).T])
        elif dim == 3:
            polynomial = np.hstack([constant, points.T, xy, yz, xz, (points**2).T, x2y, x2z,
                                    xy2, y2z, xz2, yz2, (points**3).T])
        else:
            raise ValueError("Dimension order not supported.")
    else:
        raise ValueError("Polynomial degree not supported.")

    return polynomial

                
#######################################################################################################


def dpolynomial(points, degree, deriv):
    '''
    Evaluation on the given points of the first derivative of a complete polynomial of prescribed
    degree.
    Implemented for degrees up to the third and for at most 3-D problems.
    
    Input:
    - points: ndim x npoints array
    - degree: scalar
    - deriv:  variable with respect to which the derivate is calculated (i.e. 'dx','dy' or 'dz')
    
    Output:
    - polynomial: npoints x nmonomials
    '''
    dim       = points.shape[0]
    npoints   = points.shape[-1] 
    constant  = np.ones([npoints, 1])
    dconstant = np.zeros([npoints, 1])
        
    if dim > 1:
        x         = np.ones([npoints, 1])
        y         = np.ones([npoints, 1])
        x[:,0]    = points[0]
        y[:,0]    = points[1]
        xy        = np.multiply(x,y)    
    if dim == 3:
        z         = np.ones([npoints, 1])
        z[:,0]    = points[2]
        yz        = np.multiply(y,z)
        xz        = np.multiply(x,z)
        
    if degree == 0:
        polynomial = np.hstack([dconstant])
    elif degree == 1:
        if dim == 1:
            polynomial = np.hstack([dconstant, constant])
        elif dim == 2:
            if deriv == 'dx':
                polynomial = np.hstack([dconstant, constant, dconstant])
            elif deriv == 'dy':
                polynomial = np.hstack([dconstant, dconstant, constant])
            else:
                raise ValueError("Invalid derivative.")
        elif dim == 3:
            if deriv == 'dx':
                polynomial = np.hstack([dconstant, constant, dconstant, dconstant])
            elif deriv == 'dy':
                polynomial = np.hstack([dconstant, dconstant, constant, dconstant])
            elif deriv == 'dz':
                polynomial = np.hstack([dconstant, dconstant, dconstant, constant])
            else:
                raise ValueError("Invalid derivative.")
        else: 
            raise ValueError("Dimension order not supported.")
    elif degree == 2:
        if dim == 1:
            polynomial = np.hstack([dconstant, constant, 2*points.T])
        elif dim == 2:
            if deriv == 'dx':
                polynomial = np.hstack([dconstant,constant, dconstant, y, 2*x, dconstant])
            elif deriv == 'dy':
                polynomial = np.hstack([dconstant, dconstant, constant, x, dconstant, 2*y])
            else:
                raise ValueError("Invalid derivative.")
        elif dim == 3:
            if deriv == 'dx':
                polynomial = np.hstack([dconstant,constant, dconstant, dconstant, 
                                        y, dconstant, z, 2*x, dconstant, dconstant])
            elif deriv == 'dy':
                polynomial = np.hstack([dconstant, dconstant, constant, dconstant, 
                                        x, z, dconstant, dconstant, 2*y, dconstant])
            elif deriv == 'dz':
                polynomial = np.hstack([dconstant, dconstant, dconstant, constant,
                                        dconstant, y, x, dconstant, dconstant, 2*z])
            else:
                raise ValueError("Invalid derivative.")
        else:
            raise ValueError("Dimension order not supported.")
    elif degree == 3:
        if dim == 1:
            polynomial = np.hstack([dconstant, constant, 2*points.T, (3*points**2).T])
        elif dim == 2:
            if deriv == 'dx':
                polynomial = np.hstack([dconstant, constant, dconstant, y, 2*x, dconstant, 2*xy, y**2,
                                       3*x**2, dconstant])
            elif deriv == 'dy':
                polynomial = np.hstack([dconstant, dconstant, constant, x, dconstant, 2*y, x**2, 2*xy,
                                       dconstant, 3*y**2])
            else:
                raise ValueError("Invalid derivative.")
        elif dim == 3:
            if deriv == 'dx':
                polynomial = np.hstack([dconstant,constant, dconstant, dconstant, y, dconstant, z, 
                                        2*x, dconstant, dconstant, 2*xy, 2*xz, y**2, dconstant, z**2,
                                       dconstant, 3*x**2, dconstant, dconstant])
            elif deriv == 'dy':
                polynomial = np.hstack([dconstant,dconstant, constant, dconstant, x, z, dconstant, 
                                        dconstant, 2*y, dconstant, 2*x**2, dconstant, 2*xy, 2*yz,
                                        dconstant, z**2, dconstant, 3*y**2, dconstant])
            elif deriv == 'dz':
                polynomial = np.hstack([dconstant,dconstant, dconstant, constant, dconstant, y, x, 
                                        dconstant, dconstant, 2*z, dconstant, x**2, dconstant, y**2,
                                        2*xz, 2*yz, dconstant, dconstant, 3*z**2])
            else:
                raise ValueError("Invalid derivative.")
        else:
            raise ValueError("Dimension order not supported.")
    else: 
        raise ValueError("Polynomial degree not supported.")
    
    return polynomial


#######################################################################################################


def expand_linear_system(A, P):
    '''
    Given a matrix A (n x n) and a polynomial (n x m), build the square matrix:
    
    A  P
    
    P' 0
    '''
    dim   = P.shape[-1]
    ZEROS = np.zeros([dim, dim])
    newA  = np.hstack([A, P])
    newA  = np.vstack([newA,np.hstack([P.T, ZEROS])])
    
    return newA
    
    
#######################################################################################################


def expand_vector(f, P):
    '''
    Given a vector f (n x 1) and a polynomial (n x m), expand f with zeros so to allow
    premultiplication with P
    '''
    dim   = P.shape[-1]
    zeros = np.zeros([dim])
    newdi = np.hstack([f.T, zeros.T])
    
    return newdi


#######################################################################################################


def select_region(coords, patch):
    '''
    Function for selecting points within the original dataset distribution.
    '''
    get_ipython().magic(u'matplotlib qt')
    coords = coords.T
    fig = plt.figure(figsize=(8,8))
    plt.scatter(coords[:,0],coords[:,1]);
    M   = np.max(coords)
    plt.xlim([-M*1.2, M*1.2])
    plt.ylim([-M*1.2, M*1.2])
    fig = plt.gcf()
    
    fig.show()
    plt.pause(1e-6)
    
    if patch == 'rectangle':
        marker_scaffold = plt.ginput(2)
    elif patch == 'circle':
        center, peri    = plt.ginput(2)
        radius          = np.linalg.norm(np.array(center) - np.array(peri))
        marker_scaffold = (center, radius)
    else:
        raise ValueError("Invalid patch.")
    
    if patch == 'rectangle':
        width  = marker_scaffold[1][0] - marker_scaffold[0][0]
        height = marker_scaffold[1][1] - marker_scaffold[0][1]
        el     = patches.Rectangle((marker_scaffold[0]), width, height,
                                        edgecolor='r', lw=2, facecolor='none')
    elif patch == 'circle': 
        el = patches.Circle((marker_scaffold[0]), marker_scaffold[1], edgecolor='r',
                            lw=2, facecolor='none')
    else:
        raise ValueError("Invalid patch.")
        
    index = list()
    
    if patch == 'rectangle':
        x1   = marker_scaffold[0][0]
        x2   = marker_scaffold[1][0]
        y1   = marker_scaffold[0][1]
        y2   = marker_scaffold[1][1]
        xmin = np.min([x1,x2])
        xmax = np.max([x1,x2])
        ymin = np.min([y1,y2])
        ymax = np.max([y1,y2])
        
        for i in range(len(coords)):
            x = coords[:,0]
            y = coords[:,1]
            if x[i] <= xmax and x[i] >= xmin and y[i] <= ymax and y[i] >= ymin:
                index.append(i)    
        
    if patch == 'circle':
        center = marker_scaffold[0]
        radius = marker_scaffold[1]
        
        for i in range(len(coords)):
            if euclidean_norm(center,coords[i]) <= radius:
                    index.append(i)
                    
    points = coords[index]
    
    get_ipython().magic(u'matplotlib inline')
    fp = {'family':'serif', 'serif':['Helvetica'], 'size':'18'}
    rc('font', **fp)
    rc('text',  usetex=True)
    rc('xtick', labelsize=12)
    rc('ytick', labelsize=12)
    plt.rcParams['figure.figsize'] = (10.0, 7.0)
    plt.rcParams['axes.grid'] = True
    fig = plt.figure(figsize=(8,8))
    ax  = fig.add_subplot(111)
    plt.scatter(coords[:,0],coords[:,1], c='b');
    plt.scatter(points[:,0],points[:,1], c='r');
    M   = np.max(coords)
    plt.xlim([-M*1.2, M*1.2])
    plt.ylim([-M*1.2, M*1.2])
    ax.add_artist(el)
    
    return index


#######################################################################################################


def LSM_displ(coords_ref, coords_def, config):
    '''
    Least square method for linear polynomial hyperplane approximation.
    '''
    # init input parameters
    displ      = coords_def - coords_ref
    
    if config == 'reference':
        # length of array
        len_coords = len(coords_ref)
        DIM        = coords_ref.shape[-1]

        # build linear system for least squares
        sys1 = np.c_[np.ones((len_coords)),coords_ref]
        dim  = sys1.shape[-1]

        M  = np.hstack((sys1)).reshape((len_coords, dim))
        cx = displ[:,0].reshape((len_coords,1))
        cy = displ[:,1].reshape((len_coords,1))

        # solve least squares problem using numpy
        Kx, _, _, _   = np.linalg.lstsq(M, cx)
        res_pts_ux    = np.linalg.norm((np.dot(M, Kx) - cx).reshape((len(M), 1)), axis=1)
        Ky, _, _, _   = np.linalg.lstsq(M, cy)
        res_pts_uy    = np.linalg.norm((np.dot(M, Ky) - cy).reshape((len(M), 1)), axis=1)
        A             = Kx.reshape(dim,1)
        B             = Ky.reshape(dim,1)
    elif config == 'current':
        # length of array
        len_coords = len(coords_def)
        DIM        = coords_def.shape[-1]

        # build linear system for least squares
        sys1 = np.c_[np.ones((len_coords)),coords_def]
        dim  = sys1.shape[-1]

        M  = np.hstack((sys1)).reshape((len_coords, dim))
        cx = displ[:,0].reshape((len_coords,1))
        cy = displ[:,1].reshape((len_coords,1))

        # solve least squares problem using numpy
        Kx, _, _, _   = np.linalg.lstsq(M, cx)
        res_pts_ux    = np.linalg.norm((np.dot(M, Kx) - cx).reshape((len(M), 1)), axis=1)
        Ky, _, _, _   = np.linalg.lstsq(M, cy)
        res_pts_uy    = np.linalg.norm((np.dot(M, Ky) - cy).reshape((len(M), 1)), axis=1)
        A             = Kx.reshape(dim,1)
        B             = Ky.reshape(dim,1)
    else:
        raise ValueError("Invalid configuration.")

    return A,B,res_pts_ux, res_pts_uy


#######################################################################################################


def LSM2_displ(coords_ref, coords_def, config):
    '''
    Least square method for quadratic polynomial hyperplane approximation.
    '''
    # init input parameters
    displ      = coords_def - coords_ref
    
    if config == 'reference':
        # length of array
        len_coords = len(coords_ref)
        DIM        = coords_ref.shape[-1]

        # build linear system for least squares
        sys1 = np.c_[np.ones((len_coords)),coords_ref, coords_ref**2]
        dim  = sys1.shape[-1]

        M  = np.hstack((sys1)).reshape((len_coords, dim))
        cx = displ[:,0].reshape((len_coords,1))
        cy = displ[:,1].reshape((len_coords,1))

        # solve least squares problem using numpy
        Kx, _, _, _   = np.linalg.lstsq(M, cx)
        res_pts_ux    = np.linalg.norm((np.dot(M, Kx) - cx).reshape((len(M), 1)), axis=1)
        Ky, _, _, _   = np.linalg.lstsq(M, cy)
        res_pts_uy    = np.linalg.norm((np.dot(M, Ky) - cy).reshape((len(M), 1)), axis=1)
        A             = Kx.reshape(dim,1)
        B             = Ky.reshape(dim,1)
    elif config == 'current':
        # length of array
        len_coords = len(coords_def)
        DIM        = coords_def.shape[-1]

        # build linear system for least squares
        sys1 = np.c_[np.ones((len_coords)),coords_def, coords_def**2]
        dim  = sys1.shape[-1]

        M  = np.hstack((sys1)).reshape((len_coords, dim))
        cx = displ[:,0].reshape((len_coords,1))
        cy = displ[:,1].reshape((len_coords,1))

        # solve least squares problem using numpy
        Kx, _, _, _   = np.linalg.lstsq(M, cx)
        res_pts_ux    = np.linalg.norm((np.dot(M, Kx) - cx).reshape((len(M), 1)), axis=1)
        Ky, _, _, _   = np.linalg.lstsq(M, cy)
        res_pts_uy    = np.linalg.norm((np.dot(M, Ky) - cy).reshape((len(M), 1)), axis=1)
        A             = Kx.reshape(dim,1)
        B             = Ky.reshape(dim,1)
    else:
        raise ValueError("Invalid configuration.")

    return A,B,res_pts_ux, res_pts_uy


#######################################################################################################


def mechanical_regularization(coords_ref, coords_def, radius, order, config):
    '''
    Function for smoothing out the error in the displacement measures. In a close neighborhood
    of each point a LSM is computed so to approximate the displacement field with an hyperplane of
    chosen order. As output, new regularized ux and uy at each point are given. 
    '''
    if not len(coords_ref) == len(coords_def):
        raise ValueError("Arrays must be of equal length.")
    
    n_pts       = len(coords_ref)
    displ       = coords_def-coords_ref
    ux          = displ[:,0]
    uy          = displ[:,1]
    r_homo      = radius
    ux_smoothed = list()
    uy_smoothed = list()
    
    if config == 'reference':
        for i in range(n_pts):
            pt_i     = coords_ref[i,:]
            index    = list()

            for k in range(len(coords_ref)):
                if euclidean_norm(pt_i,coords_ref[k,:]) <= r_homo:
                    index.append(k)

            points     = coords_ref[index]
            n_hpts     = len(points)
            ux_pts     = ux[index].reshape(n_hpts,1)
            uy_pts     = uy[index].reshape(n_hpts,1)
            coords_cur = coords_def[index]

            if order == 'linear':
                A,B,res_ux,res_uy    = LSM_displ(points, coords_cur, config)
                X                    = np.hstack([1,pt_i])
                UX                   = np.dot(X,A)
                UY                   = np.dot(X,B)
                ux_smoothed.append(UX)
                uy_smoothed.append(UY)
            elif order == 'quadratic':
                A,B,res_norm,res_pts = LSM2_displ(points, coords_cur, config)
                X                    = np.hstack([1,pt_i,pt_i**2])
                UX                   = np.dot(X,A)
                UY                   = np.dot(X,B)
                ux_smoothed.append(UX)
                uy_smoothed.append(UY)    
            else:
                raise ValueError("Invalid polynomial order of the hyperplane.")
        ux = np.array(ux_smoothed)
        uy = np.array(uy_smoothed)
    elif config == 'current':
        for i in range(n_pts):
            pt_i     = coords_def[i,:]
            index    = list()

            for k in range(len(coords_def)):
                if euclidean_norm(pt_i,coords_def[k,:]) <= r_homo:
                    index.append(k)

            points     = coords_def[index]
            n_hpts     = len(points)
            ux_pts     = ux[index].reshape(n_hpts,1)
            uy_pts     = uy[index].reshape(n_hpts,1)
            coords_zer = coords_ref[index]

            if order == 'linear':
                A,B,res_ux,res_uy    = LSM_displ(coords_zer, points, config)
                X                    = np.hstack([1,pt_i])
                UX                   = np.dot(X,A)
                UY                   = np.dot(X,B)
                ux_smoothed.append(UX)
                uy_smoothed.append(UY)
            elif order == 'quadratic':
                A,B,res_norm,res_pts = LSM2_displ(coords_zer, points, config)
                X                    = np.hstack([1,pt_i,pt_i**2])
                UX                   = np.dot(X,A)
                UY                   = np.dot(X,B)
                ux_smoothed.append(UX)
                uy_smoothed.append(UY)    
            else:
                raise ValueError("Invalid polynomial order of the hyperplane.")
        ux = np.array(ux_smoothed)
        uy = np.array(uy_smoothed)
    else:
        raise ValueError("Invalid configuration.")
    
    return ux,uy


#######################################################################################################


def make_heights_equal(fig, rect, ax1, ax2, ax3, pad):
    # pad in inches

    h1, v1 = Size.AxesX(ax1), Size.AxesY(ax1)
    h2, v2 = Size.AxesX(ax2, 0.1), Size.AxesY(ax2)
    h3, v3 = Size.AxesX(ax3), Size.AxesY(ax3)

    pad_v = Size.Scaled(1)
    pad_h = Size.Fixed(pad)

    my_divider = HBoxDivider(fig, rect,
                             horizontal=[h1, pad_h, h2, pad_h, h3],
                             vertical=[v1, pad_v, v2, pad_v, v3])


    ax1.set_axes_locator(my_divider.new_locator(0))
    ax2.set_axes_locator(my_divider.new_locator(2))
    ax3.set_axes_locator(my_divider.new_locator(4))


#######################################################################################################


def create_grids(coords, density, offset):
    '''
    Create regular square grids X and Y over the set of points defined by coords
    '''
    
    coords_x   = coords[:,0]
    coords_y   = coords[:,1]
    x1         = np.min(coords_x)
    x2         = np.max(coords_x)
    y1         = np.min(coords_y)
    y2         = np.max(coords_y)
    tx         = np.linspace(x1+offset, x2-offset, density)
    ty         = np.linspace(y1+offset, y2-offset, density)
    X, Y       = np.meshgrid(tx,ty)
    
    return X,Y



#######################################################################################################


def create_mask(X,Y,image,threshold):
    '''
    Create a mask to adapt plotting grids to the shape/borders of the ROI of the image
    '''
    x1                           = np.min(X)
    x2                           = np.max(X)
    y1                           = np.min(Y)
    y2                           = np.max(Y)
    Xmax                         = int(y2)
    Xmin                         = int(y1)
    Ymax                         = int(x2)
    Ymin                         = int(x1)
    img_old                      = cv2.imread(image);
    img_new                      = np.zeros(img_old.shape,np.uint8);
    img_new[Xmin:Xmax,Ymin:Ymax] = img_old[Xmin:Xmax,Ymin:Ymax];
    img                          = img_new

    img             = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY);
    thresh, im_bw   = cv2.threshold(img, threshold, 255, cv2.THRESH_BINARY);
    lower           = 0;
    upper           = 1;
    edged           = cv2.Canny(im_bw, lower, upper)
    shapeMask       = cv2.inRange(im_bw, lower, upper);
    cnts, hierarchy = cv2.findContours(edged, cv2.RETR_LIST, cv2.CHAIN_APPROX_SIMPLE);

    if not np.sum(hierarchy):
        reshape = 0
        mask    = 0
    else:
        reshape = 1
        n_contours = hierarchy.shape[1]

        for i in range(n_contours):
            points   = np.array(cnts)[i]

            if len(points) <= 2:
                    status = 0
            else:
                status = 1

            if status == 1:
                x          = points[:,0,0].reshape(len(points),1)
                y          = points[:,0,1].reshape(len(points),1)
                shape      = np.hstack([x, y])
                X,Y        = adapt_grid(X,Y,shape,direction='out');
                mask       = X*0
    
    return mask


#######################################################################################################


def select_region_on_image(coords, image, patch):
    '''
    Function for selecting points within the original dataset distribution.
    '''
    get_ipython().magic(u'matplotlib qt')
    fig    = plt.figure(figsize=(8,8))
    plt.scatter(coords[:,0],coords[:,1]);
    plt.imshow(plt.imread(image), plt.cm.gray);
    fig    = plt.gcf()
    
    fig.show()
    plt.pause(1e-6)
    
    if patch == 'rectangle':
        marker_scaffold = plt.ginput(2)
    elif patch == 'circle':
        center, peri    = plt.ginput(2)
        radius          = np.linalg.norm(np.array(center) - np.array(peri))
        marker_scaffold = (center, radius)
    else:
        raise ValueError("Invalid patch.")
    
    if patch == 'rectangle':
        width  = marker_scaffold[1][0] - marker_scaffold[0][0]
        height = marker_scaffold[1][1] - marker_scaffold[0][1]
        el     = patches.Rectangle((marker_scaffold[0]), width, height,
                                        edgecolor='r', lw=2, facecolor='none')
    elif patch == 'circle': 
        el = patches.Circle((marker_scaffold[0]), marker_scaffold[1], edgecolor='r',
                            lw=2, facecolor='none')
    else:
        raise ValueError("Invalid patch.")
        
    index = list()
    
    if patch == 'rectangle':
        x1   = marker_scaffold[0][0]
        x2   = marker_scaffold[1][0]
        y1   = marker_scaffold[0][1]
        y2   = marker_scaffold[1][1]
        xmin = np.min([x1,x2])
        xmax = np.max([x1,x2])
        ymin = np.min([y1,y2])
        ymax = np.max([y1,y2])
        
        for i in range(len(coords)):
            x = coords[:,0]
            y = coords[:,1]
            if x[i] <= xmax and x[i] >= xmin and y[i] <= ymax and y[i] >= ymin:
                index.append(i)    
        
    if patch == 'circle':
        center = marker_scaffold[0]
        radius = marker_scaffold[1]
        
        for i in range(len(coords)):
            if euclidean_norm(center,coords[i]) <= radius:
                    index.append(i)
                    
    points = coords[index]
    
    get_ipython().magic(u'matplotlib inline')
    fp = {'family':'serif', 'serif':['Helvetica'], 'size':'18'}
    rc('font', **fp)
    rc('text',  usetex=True)
    rc('xtick', labelsize=12)
    rc('ytick', labelsize=12)
    plt.rcParams['figure.figsize'] = (10.0, 7.0)
    plt.rcParams['axes.grid'] = True
    fig = plt.figure(figsize=(8,8))
    ax  = fig.add_subplot(111)
    plt.scatter(coords[:,0],coords[:,1], c='b');
    plt.scatter(points[:,0],points[:,1], c='r');
    plt.imshow(plt.imread(image), plt.cm.gray);
    ax.add_artist(el)
    
    return index

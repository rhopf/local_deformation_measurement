# required modules and functions
from __future__ import division
from scipy.stats import linregress
from ldm.helper import *

# plotting
from matplotlib import pyplot as plt
from matplotlib import rc
fp = {'family':'serif', 'serif':['Helvetica'], 'size':'20'}
rc('font', **fp)
rc('text',  usetex=True)
rc('xtick', labelsize=20)
rc('ytick', labelsize=20)

# plot scaling
plt.rcParams['figure.figsize'] = (10.0, 7.0)
plt.rcParams['axes.grid'] = True

# modules
import cv2
import sys, os, glob
import numpy as np

# ipython widgets
from ipywidgets import widgets
from IPython.display import display


def set_marker_area(frames, **kwargs):
    '''
    Provide area where tracker markers are going to be defined.
    '''

    # set vars
    marker_mode = kwargs['marker_mode']

    # read and convert nth image
    img = cv2.cvtColor(cv2.imread(frames[0]), cv2.COLOR_BGR2GRAY)
   
    # image dimensions
    len_y = np.max(img.shape[0])
    len_x = np.max(img.shape[1])

    # show image
    plt.imshow(img, cmap=plt.cm.gray)

    # add some basic guides
    plt.plot([0, len_x],[len_y, 0], 'b', lw=0.5)
    plt.plot([len_x, 0],[len_y, 0], 'b', lw=0.5)
    plt.plot([0, len_x],[len_y-int(len_y*(1/3.0)), len_y-int(len_y*(1/3.0))], 'r', lw=0.5)
    plt.plot([0, len_x],[len_y-int(len_y*(1/2.0)), len_y-int(len_y*(1/2.0))], 'r', lw=0.5)
    plt.plot([0, len_x],[len_y-int(len_y*(2/3.0)), len_y-int(len_y*(2/3.0))], 'r', lw=0.5)
    plt.plot([int(len_x*(1/3.0)), int(len_x*(1/3.0))],[len_y, 0], 'r', lw=0.5)
    plt.plot([int(len_x*(1/2.0)), int(len_x*(1/2.0))],[len_y, 0], 'r', lw=0.5)
    plt.plot([int(len_x*(2/3.0)), int(len_x*(2/3.0))],[len_y, 0], 'r', lw=0.5)
    plt.xlim([0, len_x])
    plt.ylim([len_y, 0])
    plt.grid(None)
    
    # make sure window is active
    fig = plt.gcf()
    fig.show()
    plt.pause(1e-6)
    fig.canvas.manager.window.showMaximized()

    if marker_mode.value == r'Rectangle':
       
        # seed rectangle
        marker_scaffold = plt.ginput(2)

    if marker_mode.value == r'Circle (Center/Radius)':
        
        # seed circle
        center, peri = plt.ginput(2)
        radius = np.linalg.norm(np.array(center) - np.array(peri))
        marker_scaffold = (center, radius)

    if marker_mode.value == r'Circle (3 Points)':

        # seed circle
        pt1, pt2, pt3 = plt.ginput(3)
        x1, y1 = pt1
        x2, y2 = pt2
        x3, y3 = pt3

        # calculate circle
        xm = (x1**2*y2 - x1**2*y3 - x2**2*y1 + x2**2*y3 + x3**2*y1 - x3**2*y2 + y1**2*y2 - 
              y1**2*y3 - y1*y2**2 + y1*y3**2 + y2**2*y3 - y2*y3**2)/ (2*(x1*y2 - x1*y3 - x2*y1 + x2*y3 + x3*y1 - x3*y2))
        ym = (-x1**2*x2 + x1**2*x3 + x1*x2**2 - x1*x3**2 + x1*y2**2 - x1*y3**2 - x2**2*x3 + 
               x2*x3**2 - x2*y1**2 + x2*y3**2 + x3*y1**2 - x3*y2**2)/(2*(x1*y2 - x1*y3 - x2*y1 + x2*y3 + x3*y1 - x3*y2))
        radius = kwargs['r_scaler']*np.abs(np.sqrt((x1**2 - 2*x1*x2 + x2**2 + y1**2 - 2*y1*y2 + y2**2)*
                                (x1**2 - 2*x1*x3 + x3**2 + y1**2 - 2*y1*y3 + y3**2)*
                                (x2**2 - 2*x2*x3 + x3**2 + y2**2 - 2*y2*y3 + y3**2))/
                                (2*np.abs(x1*y2 - x1*y3 - x2*y1 + x2*y3 + x3*y1 - x3*y2)))

        center = (xm, ym)
        marker_scaffold = (center, radius)
        
    if marker_mode.value == r'Manual':
        
        # define button for escape and plot it
        a = int(len_x/10.0)
        b = int(len_y/10.0)
        plt.plot([0, a], [0, 0], 'r', lw=4)
        plt.plot([0, a], [b, b], 'r', lw=4)
        plt.plot([0, 0], [0, b], 'r', lw=4)
        plt.plot([a, a], [0, b], 'r', lw=4)

        marker_scaffold = []
        while(True):
            p = plt.ginput(1)
            x = p[0][0]
            y = p[0][1]
            plt.plot([x], [y], 'co', ms=4)
            if x < a and y < b:
                break
            marker_scaffold.append(p[0])

    # close window
    plt.close()

    return marker_scaffold, img


def set_markers(marker_scaffold, frames, feature_params, **kwargs):
    '''
    Set makers for tracking, based on previous mouse input.
    '''

    # set vars
    marker_mode = kwargs['marker_mode']
    detect_features = kwargs['detect_features']
    m_spacing = kwargs['m_spacing']
    n_spacing = kwargs['n_spacing']
    
    # set color lists
    color_green  = [0, 255, 0]
    color_red    = [0, 0, 255]
    color_yellow = [0, 255, 255]

    # load image
    img = cv2.cvtColor(cv2.imread(frames[0]), cv2.COLOR_BGR2GRAY)

    # image dimensions
    len_y = np.max(img.shape[0])
    len_x = np.max(img.shape[1])

    # make masks for automatic corner detection area
    mask_center = np.zeros(img.shape, np.uint8)

    if marker_mode.value == r'Rectangle':
        
        # unpack params
        topleft, bottomright = marker_scaffold
        
        # create rectangular mask
        x1 = np.min([int(topleft[0]), int(bottomright[0])])
        x2 = np.max([int(topleft[0]), int(bottomright[0])])
        y1 = np.min([int(topleft[1]), int(bottomright[1])])
        y2 = np.max([int(topleft[1]), int(bottomright[1])])
        mask_center[y1:y2, x1:x2] = 255
        
        if detect_features.value == 'Yes':
            p0 = cv2.goodFeaturesToTrack(img, mask = mask_center, **feature_params)

        else:
            # create square raster
            x = np.linspace(x1, x2, np.ceil((x2-x1)/m_spacing))
            y = np.linspace(y1, y2, np.ceil((y2-y1)/n_spacing))
            xx, yy = np.meshgrid(x, y)
            p0 = np.concatenate(([xx.ravel()], [yy.ravel()]), axis=0).T
            p0.shape = (p0.shape[0], 1, p0.shape[1])
        
    if marker_mode.value == r'Circle (Center/Radius)' or marker_mode.value == r'Circle (3 Points)':

        # unpack params
        center, radius = marker_scaffold
                    
        if detect_features.value == 'Yes':
            # create full square raster as bounding box
            xb  = range(int(np.round(center[0] - radius)), int(np.round(center[0] + radius)))[::-1]
            yb  = range(int(np.round(center[1] - radius)), int(np.round(center[1] + radius)))[::-1]
            
            # create circular mask
            for x in xb:
                for y in yb:
                    pt   = np.array([x, y])
                    if np.linalg.norm(pt-center) <= radius:
                        mask_center[y, x] = 255
            
            # write points
            p0 = cv2.goodFeaturesToTrack(img, mask = mask_center, **feature_params)
            
        else:
            
            # create square raster as bounding box
            x1 = center[0] - radius
            x2 = center[0] + radius
            y1 = center[1] - radius
            y2 = center[1] + radius
            x_list  = np.linspace(x1, x2, np.ceil((x2-x1)/m_spacing))[::-1]
            y_list  = np.linspace(y1, y2, np.ceil((y2-y1)/n_spacing))[::-1]
            
            # init lists
            xx = list()
            yy = list()
            
            for x in x_list:
                for y in y_list:
                    pt   = np.array([x, y])
                    if np.linalg.norm(pt-center) <= radius:
                        xx.append(x)
                        yy.append(y)
                        
            # write points
            p0 = np.concatenate(([xx], [yy]), axis=0).T
            p0.shape = (p0.shape[0], 1, p0.shape[1])
            
    if marker_mode.value == r'Manual':

            # write points directly (copy)
            p0 = np.array(marker_scaffold)
            p0.shape = (p0.shape[0], 1, p0.shape[1])

    # ensure correct marker formatting
    p0       = p0.astype(np.float32)

    # draw the markers (temp reload img required)
    img = cv2.imread(frames[0])
    plt.figure(figsize=(8, 8))
    plt.imshow(img, cmap=plt.cm.gray)
    plt.xlim([0, len_x])
    plt.ylim([len_y, 0])
    for i, new in enumerate(p0):
        a, b = new.ravel()
        cv2.circle(img, (a, b), 2, color_green, -1)

    return p0


def run_tracker(frames, p0, tracker_params):
    '''
    Run the LK optical flow tracker.
    '''

    # init lists
    coords      = list()

    # load first image
    img_old = cv2.cvtColor(cv2.imread(frames[0]), cv2.COLOR_BGR2GRAY)

    # set color lists
    color_green  = [0, 255, 0]
    color_red    = [0, 0, 255]
    color_yellow = [0, 255, 255]

    # init window thread
    # cv2.namedWindow('frame', cv2.CV_WINDOW_AUTOSIZE)

    for frame_name in frames:
        
        try:
            with open(frame_name):
               pass
        
        except IOError:
            break
        
        img_new_color = cv2.imread(frame_name)
        img_new = cv2.cvtColor(img_new_color, cv2.COLOR_BGR2GRAY)

        # calculate optical flow
        p1, st, err = cv2.calcOpticalFlowPyrLK(img_old, img_new, p0, None, **tracker_params)

        # write points
        markers_new = p1
        markers_old = p0

        # save points
        coords.append(markers_new)

        # draw good tracks
        for i,(new, old) in enumerate(zip(markers_new[st==1], markers_old[st==1])):
            a,b = new.ravel()
            c,d = old.ravel()
            cv2.circle(img_new_color, (a,b), 1,   color_green, -1)
            cv2.line(img_new_color, (c,d), (a,b), color_yellow, 1)
            
        # draw bad tracks
        for i,(new, old) in enumerate(zip(markers_new[st==0], markers_old[st==0])):
            a,b = new.ravel()
            c,d = old.ravel()
            cv2.circle(img_new_color, (a,b), 2, color_red, -1)

        cv2.imshow('frame', img_new_color)
        cv2.waitKey(1)

        # now update the previous frame and previous points
        img_old = img_new.copy()
        p0      = markers_new.reshape(-1, 1, 2)

    cv2.waitKey(10)
    cv2.destroyAllWindows()

    # nasty hack to ensure window closing
    cv2.waitKey(10)
    cv2.waitKey(10)
    cv2.waitKey(10)
    cv2.waitKey(10)
        
    # store most recent bad indices
    idx_bad = np.where([i[0] for i in st==0])[0]

    # write to numpy array and delete bad indices
    coords = np.array(coords)
    coords = coords[:,:, 0]
    coords = np.delete(coords, idx_bad, axis=1)
    print 'Nr. of points discarded:', len(idx_bad)

    return coords

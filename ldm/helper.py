import numpy as np

# SIMPLE HELPER FUNCTIONS
def replace_in_string(string, find_this, replace_with):
    '''
    filepath: path to text file.
    '''
    for F, R in zip(find_this, replace_with):
        string = string.replace(F, R)

    return string


def chop(v, c):
    return v[c:(len(v)-c)]


def numdiff(x, y, n=1):
    
    dydx = np.diff(y, n)/np.diff(x[(n-1):], 1)**n 
    x    = 0.5*(x[n:] + x[:-n])
    return x, dydx


def numintegrate(x, y):
    
    x = np.array(x)
    y = np.array(y)
    
    dx = np.diff(x)
    yi = (y[:-1] + y[1:])/2.0
    
    return np.cumsum(yi*dx)


def branch_split(idx_split, data):
    
    # split branches
    data_l = data[:idx_split]
    data_u = data[idx_split:]
    
    # reverse unloading
    data_u = data_u[::-1]
    
    return data_l, data_u


def get_pixel_circle(center, radius):
    
    # build circle
    t = np.linspace(0, 1, 10000)
    x = np.round(radius*np.cos(2*np.pi*t) + center[0]).astype(int)
    y = np.round(radius*np.sin(2*np.pi*t) + center[1]).astype(int)
    
    xnew = []
    ynew = []

    for i, (xi, yi) in enumerate(zip(x, y)):
        if x[i-1] != xi or y[i-1] != yi:
            xnew.append(xi)
            ynew.append(yi)

    xnew = np.array(xnew)
    ynew = np.array(ynew)
    
    return xnew, ynew


def cut_cycles_at_value(cycles, val, cd_dist=30):
    '''
    Function that extracts cycles at a given minimum value (i.e. preforce).
    The cooldown distance is the fudge factor that has to be evaluated case by case.
    '''
    
    # init
    N          = len(cycles)
    cycles_sep = []
    
    # indices
    idx_s = 0
    idx_e = 0

    # init cd and sign
    cooldown = -1
    sign     = 1
    for i, x in enumerate(cycles, 0):
        
        # either detect cut
        if sign*x > sign*val and cooldown < 0:
            idx_e = i
            cycles_sep.append([idx_s, idx_e+1])
            idx_s = i
            
            # reset cooldown and flip sign
            cooldown = cd_dist
            sign     = -sign
        
        # or decrement cooldown
        cooldown -= 1
            
    return cycles_sep[1::2]

# parse cluttered shitty mts file
def parse_mts_file_with_headers(path):
    
    # open file stream
    f = open((path), 'r')
        
    # add values 
    vals = list()
    for line in f:
        val = list()
        elements = line.split('\t')
        if len(elements)==5:
            for element in elements:
                try:
                    val.append(float(element))
                except ValueError:
                    pass
        if val != []:
            vals.append(val)
    
    return np.array(vals)
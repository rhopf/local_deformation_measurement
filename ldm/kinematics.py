# required modules and functions
import numpy as np

# get affine map and deformation gradient of point cluster
def get_affine_transform(coords_ref, coords_def, center=True):
    '''
    Reconstruct the affine transform:
            
            x = F*X + b
            
    based on points given in reference and deformed configuration.

    Parameters
    ----------
    coords_ref : array [N_p, 2]
        List array consisting of coordinate tuples for the points, measured
        in the reference configuration.

    coords_def : array [N_p, 2]
        List array consisting of coordinate tuples for the points, measured
        in the deformed configuration.

    Returns
    -------
    F : array [2, 2]
        In-plane deformation gradient.
    b : array [2,]
        Corresponding translation vector.
    '''

    # init lists
    M = list()
    c = list()
    
    # length of array
    len_coords = len(coords_ref)

    # compute CG ("center") of reference cloud
    if center == True:
        center = coords_ref.mean(axis=0)
    elif center == False:
        center = np.array([0, 0])
    
    # reset shift for both clouds
    coords_ref = coords_ref - center
    coords_def = coords_def - center

    # build linear system for least squares
    sys_1 = np.c_[coords_ref, np.zeros((len_coords, 2)), 
                                        np.ones((len_coords)), 
                                        np.zeros((len_coords))]

    sys_2 = np.c_[np.zeros((len_coords, 2)), coords_ref, 
                                             np.zeros((len_coords)), 
                                             np.ones((len_coords))]
    # add and reshape
    M = np.hstack((sys_1, sys_2)).reshape((2*len_coords, 6))
    c = coords_def.reshape((2*len_coords))

    # solve least squares problem using numpy
    K, _, _, _ = np.linalg.lstsq(M, c)

    # get all residuals
    residuals = np.linalg.norm((np.dot(M, K) - c).reshape((len(M)/2, 2)), axis=1)

    # deformation gradient
    F = K[:4].reshape((2, 2))

    # shift vector
    b = K[4:]

    return F, b, residuals


def get_principal_strains(F):
    '''
    Compute principal strains and directions, based on a given in-plane
    deformation gradient.

    Parameters
    ----------
    F : array [2, 2]
        In-plane deformation gradient.

    Returns
    -------
    epsx : float
        Stretch in x direction
    epsy : float
        Stretch in y direction
    evec : array [2, 2]
        Eigenvectors, denoting the principal directions of deformation.
    '''

    # left cauchy green strain tensor
    B = np.dot(F, F.T)

    # compute eigenvalues and eigenvectors
    ev, evec = np.linalg.eigh(B)
    vec1, vec2 = evec

    # compute linear strain measures
    epsx = np.sqrt(ev[0])-1
    epsy = np.sqrt(ev[1])-1

    return epsx, epsy, evec


def get_polar_decomposition(F):
    '''
    Compute polar decomposition, based on a given in-plane
    deformation gradient.

    Parameters
    ----------
    F : array [2, 2]
        In-plane deformation gradient.

    Returns
    -------
    R : array [2, 2]
        Orthogonal tensor, representing the global rotation.
    M : array [2, 2]
        Symmetric, positive definite tensor, representing pure deformation.
    '''

    # cauchy green strain tensor
    C = np.dot(F.T, F)

    # compute eigenvalues
    ev, evec = np.linalg.eigh(C)
    l1       = np.sqrt(ev[0])
    l2       = np.sqrt(ev[1])
    e1, e2   = evec.T

    # symmetric tensor
    M = np.array([[l1*e1[0]*e1[0] + l2*e2[0]*e2[0], l1*e1[1]*e1[0] + l2*e2[1]*e2[0]], 
                  [l1*e1[0]*e1[1] + l2*e2[0]*e2[1], l1*e1[1]*e1[1] + l2*e2[1]*e2[1]]])

    # orthogonal tensor
    R = np.dot(F, np.linalg.inv(M))

    return R, M

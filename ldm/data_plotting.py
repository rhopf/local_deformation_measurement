# magics
from IPython import get_ipython

# plotting
from matplotlib import pyplot as plt
from matplotlib import rc
fp = {'family':'serif', 'serif':['Helvetica'], 'size':'20'}
rc('font', **fp)
rc('text',  usetex=True)
rc('xtick', labelsize=20)
rc('ytick', labelsize=20)

# plot scaling
plt.rcParams['figure.figsize'] = (10.0, 7.0)
plt.rcParams['axes.grid'] = True

# modules
import sys, os, glob
import numpy as np

        
def summary_plot_ua_ico(exp, fontsize=19, labelsize=16):
    '''
    Plot quick summary of uniaxial, incompressible tensile test.
    '''
    # name
    exp_name = ', '.join(exp['name'].split('_'))
    fontsize = 19

    # unpack
    time     = exp['time_img']
    epsx     = exp['epsx_loc']
    epsy     = exp['epsy_loc']
    epsy_ico = exp['epsy_ico_loc']
    sigx_cau = exp['sigx_cau_loc']
    sigx_pk1 = exp['sigx_pk1_loc']
    rbm      = exp['rbm']

    epsx_glob     = exp['epsx_glob']
    epsy_glob_ico = exp['epsy_ico_glob']
    sigx_cau_glob = exp['sigx_cau_glob']
    sigx_pk1_glob = exp['sigx_pk1_glob']

    fig = plt.figure(figsize=(20,23))

    # PLOT: kinematics -------------------------------------------------------
    ax  = fig.add_subplot(321)
    
    ax.plot(time, epsx, 'b', lw=1.25)
    ax.plot(time, epsx_glob, '--g', lw=1.2)
    ax.plot(time, epsy, 'b', lw=1.25)
    ax.plot(time, epsy_ico, '--r', lw=1.25)
    
    ax.grid(color='0.25')
    leg = ax.legend(['$\epsilon_x$ loc','$\epsilon_x$ glob','$\epsilon_y$','$\epsilon_y$ ico'],
                    loc='best', prop={'size':18}, fancybox=True)
    leg.get_frame().set_alpha(0.5)
    ax.set_title('Time: ' + exp_name)
    ax.set_ylabel('$\epsilon$ [-]')
    ax.set_xlabel('t [s]')
    ax.yaxis.label.set_fontsize(fontsize)
    ax.xaxis.label.set_fontsize(fontsize)
    ax.title.set_fontsize(fontsize)
    ax.title.set_y(1.01)
    ax.tick_params(axis='both', which='major', labelsize=labelsize)
    ax.tick_params(axis='x', pad=7)
    ax.tick_params(axis='y', pad=4)

    # PLOT: kinematics -------------------------------------------------------
    ax  = fig.add_subplot(322)
    
    ax.plot(epsx, epsy, lw=1.25)
    ax.plot(epsx, epsy_ico, '--r', lw=1.25)

    leg = ax.legend(['$\epsilon_y$','$\epsilon_y$ ico'],
                loc='best', prop={'size':18}, fancybox=True)
    leg.get_frame().set_alpha(0.5)
    ax.grid(color='0.25')
    ax.set_title('Kinematics: ' + exp_name)
    ax.set_xlabel('$\epsilon_x$ [-]')
    ax.set_ylabel('$\epsilon_y$ [-]')
    ax.yaxis.label.set_fontsize(fontsize)
    ax.xaxis.label.set_fontsize(fontsize)
    ax.title.set_fontsize(fontsize)
    ax.title.set_y(1.01)
    ax.tick_params(axis='both', which='major', labelsize=labelsize)
    ax.tick_params(axis='x', pad=7)
    ax.tick_params(axis='y', pad=4)

    # PLOT: constitutive -----------------------------------------------------
    ax  = fig.add_subplot(323)
    
    ax.plot(epsx, sigx_pk1, 'b', lw=1.25)
    ax.plot(epsx_glob, sigx_pk1_glob, '--g', lw=1.25)
    
    ### delete ###
    # ax.set_xlim([0.7, 0.9])
    # ax.set_ylim([0.6, 0.9])

    leg = ax.legend(['local','global'],
                loc='best', prop={'size':18}, fancybox=True)
    leg.get_frame().set_alpha(0.5)
    ax.grid(color='0.25')
    ax.set_title('Stress-Strain: ' + exp_name)
    ax.set_xlabel('$\epsilon_x$ [-]')
    ax.set_ylabel('Kirchhoff stress $\sigma_x$ [N/mm2]')
    ax.yaxis.label.set_fontsize(fontsize)
    ax.xaxis.label.set_fontsize(fontsize)
    ax.title.set_fontsize(fontsize)
    ax.title.set_y(1.01)
    ax.tick_params(axis='both', which='major', labelsize=labelsize)
    ax.tick_params(axis='x', pad=7)
    ax.tick_params(axis='y', pad=4)

    # PLOT: constitutive -----------------------------------------------------
    ax  = fig.add_subplot(324)

    ax.plot(epsx, sigx_cau, 'b', lw=1.25)
    ax.plot(epsx_glob, sigx_cau_glob, '--g', lw=1.25)
    
    leg = ax.legend(['local','global'],
                loc='best', prop={'size':18}, fancybox=True)
    leg.get_frame().set_alpha(0.5)
    ax.grid(color='0.25')
    ax.set_title('Stress-Strain: ' + exp_name)
    ax.set_xlabel('$\epsilon_x$ [-]')
    ax.set_ylabel('Cauchy stress $\sigma_x$ [N/mm2]')
    ax.yaxis.label.set_fontsize(fontsize)
    ax.xaxis.label.set_fontsize(fontsize)
    ax.title.set_fontsize(fontsize)
    ax.title.set_y(1.01)
    ax.tick_params(axis='both', which='major', labelsize=labelsize)
    ax.tick_params(axis='x', pad=7)
    ax.tick_params(axis='y', pad=4)

    # PLOT: rmb: rot -----------------------------------------------------
    ax  = fig.add_subplot(325)

    ax.plot(time, rbm['rot'], 'b', lw=1.25)
    
    ax.grid(color='0.25')
    ax.set_title('RBM - Rotation: ' + exp_name)
    ax.set_xlabel('time [s]')
    ax.set_ylabel('$\phi$ [deg]')
    ax.yaxis.label.set_fontsize(fontsize)
    ax.xaxis.label.set_fontsize(fontsize)
    ax.title.set_fontsize(fontsize)
    ax.title.set_y(1.01)
    ax.tick_params(axis='both', which='major', labelsize=labelsize)
    ax.tick_params(axis='x', pad=7)
    ax.tick_params(axis='y', pad=4)

    # PLOT: rbm: transl -----------------------------------------------------
    ax  = fig.add_subplot(326)

    ax.plot(rbm['transl'][:, 0], rbm['transl'][:, 1], 'b', lw=1.25)
    
    ax.grid(color='0.25')
    ax.set_title('RBM - Translation (CG): ' + exp_name)
    ax.set_xlabel('x shift [mm]')
    ax.set_ylabel('y shift [mm]')
    ax.yaxis.label.set_fontsize(fontsize)
    ax.xaxis.label.set_fontsize(fontsize)
    ax.title.set_fontsize(fontsize)
    ax.title.set_y(1.01)
    ax.tick_params(axis='both', which='major', labelsize=labelsize)
    ax.tick_params(axis='x', pad=7)
    ax.tick_params(axis='y', pad=4)

    return fig


def summary_plot_ps(data, exp_name):
    pass

def plot_homogeneity(exp, pct=95, pts=None):
    '''
    Plots homogeneity.
    '''
    # unpack
    coords = exp['coords']
    residuals = exp['residuals']

    # Basic settings
    n = 0
    scaling = 50

    # for all of the motion
    res_motion_ave = np.abs(residuals.mean(axis=0))
    res_med        = np.median(res_motion_ave)
    res_pct        = np.percentile(res_motion_ave, 95)

    # for each frame
    res_frame_med = np.percentile(residuals, 50, axis=1)
    res_frame_pct = np.percentile(residuals, pct, axis=1)
    res_frame_ave = residuals.mean(axis=1)

    # calculate center of gravity (CG) and residual weighted CG
    cg          = coords[n].mean(axis=0)
    cg_weighted = np.sum(coords[n]*np.c_[res_motion_ave, res_motion_ave], axis=0)/np.sum(res_motion_ave)

    if pts == None:
        # set to qt
        get_ipython().magic('matplotlib qt')

        # make sure window is active
        fig = plt.gcf()
        fig.show()
        plt.pause(1e-6)
        fig.canvas.manager.window.showMaximized()
        plt.imshow(plt.imread(exp['frames'][n]), plt.cm.gray)
        plt.scatter(coords[n][:, 0], coords[n][:, 1], s=scaling*res_motion_ave, c='#00FF00')
        pts = plt.ginput(2)
        plt.close

    # set to qt
    get_ipython().magic('matplotlib inline')

    plt.figure(figsize=(21, 7))
    plt.subplot(1, 3, 1)
    plt.imshow(plt.imread(exp['frames'][n]), plt.cm.gray)
    plt.scatter(coords[n][:, 0], coords[n][:, 1], s=scaling*res_motion_ave, c='#00FF00', alpha=0.5)
    
    # plot cg's
    plt.plot([cg[0]], [cg[1]], marker='o', color='#FFFF00', ms=20, label='center')
    plt.plot([cg_weighted[0]], [cg_weighted[1]], 'ro', ms=10, label='res. weighted center')
    plt.plot([cg[0], cg_weighted[0]], [cg[1], cg_weighted[1]], '-r', lw=1.25)
    plt.xlim([pts[0][0], pts[1][0]])
    plt.ylim([pts[0][1], pts[1][1]])
    plt.legend(loc='best', fontsize=16, fancybox=True, framealpha=0.5)
    plt.title('Scatter plot of residual norm (motion averaged)', y=1.03, fontsize=20)
    plt.xlabel('x [px]', fontsize=20)
    plt.ylabel('y [px]', fontsize=20)
    plt.tick_params(labelsize=18, labelcolor='k')

    # plot residuals with median
    plt.subplot(1, 3, 2)
    plt.plot(np.sort(res_motion_ave), lw=1.25, label='residual norm')
    plt.plot([0, len(res_motion_ave)], [res_pct, res_pct], label='residual norm 95\%')
    plt.legend(loc='best', fontsize=16, fancybox=True, framealpha=0.75)
    plt.title('Points: sorted residual norm (motion averaged)', y=1.03, fontsize=20)
    ax = plt.gca()
    plt.xlim(ax.get_xbound())
    plt.xlabel('point [nr]', fontsize=20)
    plt.ylabel('residual norm [px]', fontsize=20)
    plt.tick_params(labelsize=18, labelcolor='k')

    # plot residuals over time
    plt.subplot(1, 3, 3)
    plt.plot(res_frame_ave, '-b', lw=1.25, label='residual norm mean')
    plt.plot(res_frame_med, '-g', lw=1.25, label='residual norm median')
    plt.plot(res_frame_pct, '-r', lw=1.25, label='residual norm 95\%')
    plt.legend(loc='best', fontsize=16, fancybox=True, framealpha=0.75)
    plt.title('Points: residual norm (frame averaged)', y=1.03, fontsize=20)
    plt.xlabel('frame [-]', fontsize=20)
    plt.ylabel('residual norm [px]', fontsize=20)
    plt.tick_params(labelsize=18, labelcolor='k')

    plt.tight_layout()

    return pts

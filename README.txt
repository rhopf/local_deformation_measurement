# use: 
# conda install --channel https://conda.anaconda.org/menpo opencv
# to install opencv if it is missing!

# run this to activate extensions:
# conda install ipywidgets
# jupyter nbextension enable --py --sys-prefix widgetsnbextension
# conda install --channel https://conda.anaconda.org/conda-forge jupyter_nbextensions_configurator